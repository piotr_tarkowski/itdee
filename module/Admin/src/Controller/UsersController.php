<?php
namespace Admin\Controller;

use MVC\Controller\Controller;
use View\ViewModel;

class UsersController extends Controller
{
    public function indexAction()
    {
        
        //echo '<br><br>';
        $model = $this->model('Users');
        $users = $model->getAllUsers();
        //$id = $this->param('id');
        $roles = $model->getRoles();
        if(isset($_GET['param']))
        {
            $user = $model->getUser($_GET['param']);
            
            if(!empty($user)) {
                echo json_encode(['status' => 1, 'user' => $user]);
                exit();
            } else {
                echo json_encode(['status' => 0, 'message' => 'Ups, coś poszło nie tak jak trzeba.']);
                exit();
            }
            
        } 
        
        
        $this->view->users = $users;
        $this->view->role = $roles;
    }
    
    public function edycjaAction()
    {
        $model = $this->model('Users');
        $admUser = $this->model('Admin');

        if(filter_input(INPUT_POST, 'save') && filter_input(INPUT_POST, 'save') == 'Save')
        {
            $data = $_POST;
            $model->updUser([
                ':login' => filter_input(INPUT_POST,'login'),
                ':active' => filter_input(INPUT_POST,'active'),
                ':id'   => filter_input(INPUT_POST,'uid'),
            ]);
            if(filter_input(INPUT_POST,'passwd'))
            {
                $model->updPass([
                    ':passwd' => hash('sha512', filter_input(INPUT_POST,'passwd')),
                    ':id' => filter_input(INPUT_POST,'uid'),
                ]);
            }
            $model->updUserData([
                ':imie' => filter_input(INPUT_POST, 'fname'),
                ':nazwisko' => filter_input(INPUT_POST, 'sname'),
                ':email' => filter_input(INPUT_POST, 'email'),
                ':role' => filter_input(INPUT_POST, 'role'),
                ':id'   => filter_input(INPUT_POST, 'udid'),
            ]);
            
            echo json_encode(['status' => 1, 'message' => 'Dane zostały zapisane.']);
            exit();
        }
        

    }
    
    public function nowyAction()
    {
        $model = $this->model('Users');
        $roles = $model->getRoles();
        
        
        if(filter_input(INPUT_POST,'new_user') && filter_input(INPUT_POST, 'new_user') == 'Dodaj')
        {
            
            $token = md5(uniqid(rand()));
            $data = $_POST;
            
            $uid = $model->insUser([
                ':login' => filter_input(INPUT_POST,'login'),
                ':passwd' => hash('sha512', filter_input(INPUT_POST,'passwd')),
                ':active' => filter_input(INPUT_POST,'active'),
                ':token' => $token,
                ':display_name' => filter_input(INPUT_POST,'fname') . ' ' . filter_input(INPUT_POST,'lname'),
            ]);
            
            $fid = $model->insCompany([
                ':active' => 0,
            ]);
            
            $model->insUserData([
                ':imie' => filter_input(INPUT_POST, 'fname'),
                ':nazwisko' => filter_input(INPUT_POST, 'sname'),
                ':email' => filter_input(INPUT_POST, 'email'),
                ':uid' => $uid,
                ':role' => filter_input(INPUT_POST, 'role'),
                ':fid' => $fid,
            ]);
            
            
            echo json_encode(['status' => 1, 'message' => 'Wszystko przebiegło ok']);
            exit();
            
            
        }
        
        
        
        
        $this->view->roles = $roles;
        $this->view->firmy = $model->getComp();
    }
    
    public function newAction()
    {
        $model = $this->model('Users');
        $roles = $model->getRoles();
        
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Dodaj')
        {
            
            $repassword = filter_input(INPUT_POST, 'repassword');
            $password = filter_input(INPUT_POST, 'password');
            
            $token = md5(uniqid(rand()));
            
            
            $uid = $model->insUser([
                ':login' => filter_input(INPUT_POST, 'login'),
                ':passwd' => hash('sha512', $password),
                ':active' => 1,
                ':token' => $token,
            ]);
            
            
            
            
            $model->insUserData([
                ':imie' => filter_input(INPUT_POST, 'fname'),
                ':nazwisko' => filter_input(INPUT_POST, 'lname'),
                ':email' => filter_input(INPUT_POST, 'email'),
                ':uid' => $uid,
                ':role' => filter_input(INPUT_POST, 'rola'),
            ]);
            
            header('Location: ' . URL . '/admin/users');
        }

        $this->view->roles = $roles;
    }
    
    public function editAction()
    {
        $model = $this->model('Users');
        //$user = $model->getUser($this->param('id'));
        
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Zapisz')
        {
            $login = filter_input(INPUT_POST, 'login');
            $password = filter_input(INPUT_POST, 'password');
            $repassword = filter_input(INPUT_POST, 'repassword');
            $fname = filter_input(INPUT_POST, 'fname');
            $lname = filter_input(INPUT_POST, 'lname');
            $email = filter_input(INPUT_POST, 'email');
            $role = filter_input(INPUT_POST, 'rola');
            $uid = filter_input(INPUT_POST, 'uid');
            $udid = filter_input(INPUT_POST, 'udid');
//            echo '<pre>' . print_r($_POST,1) . '</pre>';
//            exit();

            $model->updUser([
                ':login' => $login,
                ':active' => 1,
                ':id'   => $uid,
            ]);
            if(filter_input(INPUT_POST, 'ch-pass-zm'))
            {
                $model->updPass([
                    ':passwd' => hash('sha512', $password),
                    ':id' => $uid,
                ]);
            }
            $model->updUserData([
                ':imie' => $fname,
                ':nazwisko' => $lname,
                ':email' => $email,
                ':role' => $role,
                ':id'   => $udid,
            ]);
            header('Location: ' . URL . '/admin/users');
        }
        

        $this->view->user = $model->getUser($this->param('param'));
        $this->view->roles = $model->getRoles();
    }
    
    public function usunAction()
    {
        
        
        if(filter_input(INPUT_POST, 'delete'))
        {
            $id = filter_input(INPUT_POST, 'id');
            $model = $this->model('Users');
            $model->delUser($id);
            $model->delUserDate($id);
            //$model->delCompany();
            
            echo json_encode(['status' => 1, 'message' => 'Uzytkownik został usunięty']);
            exit();
        }
        
    }
    
    public function searchAction()
    {
        
        $model = $this->model('Users');
        //echo mysqli_real_escape_string($_POST['search'];
        //echo 'dfg';
        //$_POST['search'] = 'p';
        if($_POST)
        {
            $name = filter_input(INPUT_POST, 'search');
            
            $users = $model->getUserByName($name);
            $html = '';
            foreach($users as $k=> $v)
            {
                //echo $k . ' ' . $v;
                $html .= '<div class="show" align="left"><a class="btn btn-link btn-xs btn-search" id="'. $v['id'] .'">' . $v["login"] . '</a>&nbsp;<br/></div>';
                
            }
            //echo '<pre>' . print_r($users,1) . '</pre>';
            echo $html;
            exit();
        }
        
    }
}
