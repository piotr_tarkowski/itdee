<?php
namespace Admin\Controller;

use View\ViewModel;
use MVC\Controller\Controller;

class PanelController extends Controller 
{
    
    public function indexAction()
    {
        $model = $this->model('panel');
        
        $panel = $model->getPanels();

        $this->view->panel = $panel;

    }
    
    public function editAction()
    {
        
        
        if(filter_input(INPUT_POST, 'submit') == 'Zapisz' )
        {
            $model = $this->model('panel');
            $data = filter_input_array(INPUT_POST);
            
            $model->updPanel($data);
            header('Location: ' . URL . '/admin/panel');
            
        }
        
    }
    
    public function imagesAction()
    {
        $msg = '';
//        echo '<br><br><br>';
//        
//        echo '<pre>' . print_r($data,1) . '</pre>';
        $model = $this->model('panel');
        
        if(filter_input(INPUT_POST, 'submit') == 'Zapisz' )
        {
            $data = filter_input_array(INPUT_POST);
            $model->updImages([
                'image' => $data['img'],
                'id' => $data['id'],
            ]);
            header('Location: ' . URL . '/admin/panel');
            
        } elseif (filter_input(INPUT_POST, 'submit') == 'Dodaj')
        {
            $id = filter_input(INPUT_POST, 'id');
            
            
            $files = $_FILES['files'];
            $allowed =  array('gif','png' ,'jpg','jpeg');
            $dir = ROOTPATH . 'public/img/upload/';

            $ext = pathinfo($files['name'], PATHINFO_EXTENSION);
            $filename = 'panel' . uniqid() . '.' . $ext;
            
            $target = $dir . $filename;

            if(in_array($ext,$allowed) ) 
            {
                if(move_uploaded_file($files['tmp_name'],$target))
                {
                    $model->updImages([
                        'image' => $filename,
                        'id' => $id
                    ]);
                    header('Location: ' . URL . '/admin/panel');
                }
            }
            
        }

        $this->view->msg = $msg;

    }
    
    public function newAction()
    {
        $msg = '';
        if(filter_input(INPUT_POST, 'submit') == 'Dodaj')
        {
            $data = filter_input_array(INPUT_POST);
            $model = $this->model('panel');
            
            
            $files = $_FILES['files'];
            $allowed =  array('gif','png' ,'jpg','jpeg');

            //$ext = pathinfo($filename, PATHINFO_EXTENSION);
            $order = $model->getMaxOrder();
            $data['order'] = $order['ord'] + 1;
            
            $dir = ROOTPATH . 'public/img/upload/';
            
            //echo '<br><br><br>';
            //echo '<pre>' . print_r($files,1) . '</pre>';
            //exit();
            $ext = pathinfo($files['name'], PATHINFO_EXTENSION);
            $filename = 'panel' . uniqid() . '.' . $ext;
            $target = $dir . $filename;

            if(in_array($ext,$allowed) ) 
            {
                
                if(move_uploaded_file($files['tmp_name'], $target))
                {
                    $data['image'] = $filename;

                    $model->insPanel($data);
                    header('Location: ' . URL . '/admin/panel');
                } else {
                    $msg = 'Coś puszło źle';
                }
            } else {
                $msg = 'Zły format pliku!';
            }

            header('Location: ' . URL . '/admin/panel');
        }

        $this->view->msg = $msg;

    }
    
    public function delimgAction()
    {
        
        
        if(filter_input(INPUT_POST, 'path'))
        {
            $file = ROOTPATH . 'public/img/upload/';
            $file .= filter_input(INPUT_POST, 'path');
            unlink($file);
        }
    }
    
    public function deleteAction()
    {
        
        if(!empty($this->param('param')))
        {
            $id = $this->param('param');
            $model = $this->model('panel');
            
            $pan = $model->getPanel($id);
            $path = ROOTPATH . 'public/img/upload/';
            unlink($path . $pan['image']);
            $model->delPanel($id);
            header('Location: ' . URL . '/admin/panel');
        }
    }
    
    public function orderAction()
    {
        
        if(filter_input(INPUT_POST, 'submit') == 'yes')
        {
            $model = $this->model('panel');
            $data = filter_input_array(INPUT_POST);
            $model->updOrder([
                ':orders'     => $data['orders'],
                ':id'       => $data['id'],
            ]);
            
        }
    }
    
}
