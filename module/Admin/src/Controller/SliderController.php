<?php
namespace Admin\Controller;

use View\ViewModel;
use MVC\Controller\Controller;

class SliderController extends Controller 
{
    
    public function indexAction()
    {
        $model = $this->model('Slider');
        $slider = $model->getSlider();
        
        
        $this->view->slider = $slider;

    }
    
    public function orderAction()
    {
        
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'yes')
        {
            $model = $this->model('Slider');
            $data = filter_input_array(INPUT_POST);
            $model->updOrder([
                ':orders'     => $data['orders'],
                ':id'       => $data['id'],
            ]);
            
        }
    }
    
    public function editAction()
    {
        
        
        if(filter_input(INPUT_POST, 'submit') == 'Zapisz' )
        {
            $model = $this->model('Slider');
            $data = filter_input_array(INPUT_POST);
            //echo '<br><br><br>';
            //echo '<pre>' . print_r($data, 1) . '</pre>';
            $model->updSlider($data);
            header('Location: ' . URL . '/admin/slider');
            
        }
        
    }
    
    public function imagesAction()
    {
        $msg = '';
//        echo '<br><br><br>';
//        
//        echo '<pre>' . print_r($data,1) . '</pre>';
        $model = $this->model('Slider');
        
        if(filter_input(INPUT_POST, 'submit') == 'Zapisz' )
        {
            $data = filter_input_array(INPUT_POST);
            $model->updImages([
                ':file' => $data['img'],
                ':id' => $data['id'],
            ]);
            header('Location: ' . URL . '/admin/slider');
            
        } elseif(filter_input(INPUT_POST, 'submit') == 'Delete'){
            $img = filter_input(INPUT_POST, 'img');
            $dir = ROOTPATH . 'public/img/upload/';
            $file = $dir . $img;
            unlink($file);
            header('Location: ' . URL . '/admin/slider');
        } elseif (filter_input(INPUT_POST, 'submit') == 'Dodaj')
        {
            $id = filter_input(INPUT_POST, 'id');
            
            
            $files = $_FILES['files'];
            $allowed =  array('gif', 'png' ,'jpg');
            $filename = $files['name'];
            $dir = ROOTPATH . 'public/img/upload/';
            $target = $dir . $filename;
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(file_exists($target))
            {
                $filename = 'slide' . uniqid() . '.' . $ext;
                $target = $dir . $filename;
            }
            if(in_array($ext,$allowed) ) 
            {
                if(move_uploaded_file($files['tmp_name'],$target))
                {
                    $model->updImages([
                        ':file' => $filename,
                        ':id' => $id
                    ]);
                    header('Location: ' . URL . '/admin/slider');
                } else {
                    $msg = 'Coś puszło źle';
                }
            } else {
                $msg = 'Zły format pliku!';
            }
        }


        $this->view->msg = $msg;

    }
    
    public function newAction()
    {
        
        if (filter_input(INPUT_POST, 'submit') == 'Dodaj')
        {
            $data = filter_input_array(INPUT_POST);
            $model = $this->model('Slider');
            
            $files = $_FILES['files'];
            $allowed =  array('gif', 'png', 'jpg');
            $filename = $files['name'];
            //$ext = pathinfo($filename, PATHINFO_EXTENSION);
            $order = $model->getMaxOrder();
            $data['order'] = $order['ord'] + 1;
            
            $dir = ROOTPATH . 'public/img/upload/';
            $target = $dir . $filename;
            echo '<br><br><br>';
            //echo '<pre>' . print_r($files,1) . '</pre>';
            //exit();
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(file_exists($target))
            {
                $filename = 'slide' . uniqid() . '.' . $ext;
                $target = $dir . $filename;
            }
            if(in_array($ext,$allowed) ) 
            {
                
                if(move_uploaded_file($files['tmp_name'], $target))
                {
                    $data['file'] = $filename;
                    $model->insSlider($data);
                    header('Location: ' . URL . '/admin/slider');
                } else {
                    $msg = 'Coś puszło źle';
                }
                
            } else {
                $msg = 'Zły format pliku!';
            }
            
        }
    }
	
	public function delimgAction()
    {
        
        
        if(filter_input(INPUT_POST, 'path'))
        {
            $file = ROOTPATH . 'public/img/upload/';
            $file .= filter_input(INPUT_POST, 'path');
            unlink($file);
        }
    }
    
    public function deleteAction()
    {
        
        if(!empty($this->param('param')))
        {
            $id = $this->param('param');
            $model = $this->model('Slider');
            $model->delSlider($id);
            header('Location: ' . URL . '/admin/slider');
        }
    }
}
