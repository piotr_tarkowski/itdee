<?php
namespace Admin\Controller;

use MVC\Controller\Controller;
use View\ViewModel;

class PagesController extends Controller
{
    private $modul =  [ 'contact' => 'Kontakt', 'test' => 'Test' ];

    
    public function indexAction() 
    {
        $model = $this->model('Pages');
        
        $posts = $model->getPosts();

        $this->view->posts = $posts;

    }
    
    public function newAction()
    {
        $model = $this->model('Pages');
        $users = $model->getUsers();
        
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Dodaj')
        {
            //echo '<pre>' . print_r($_POST, 1) . '</pre>';
            $data = filter_input_array(INPUT_POST);
            if(empty($data['modul']))
            {
                $data['modul'] = null;
            }
            $model->insPosts([
                ':title'            => $data['title'],
                ':name'             => $data['name'],
                ':content'          => $data['content'],
                ':type'             => 'page',
                ':status'           => $data['status'],
                ':meta_title'       => $data['meta_title'],
                ':meta_keywords'    => $data['meta_keywords'],
                ':meta_description' => $data['meta_description'],
                ':author'           => $data['author'],
                ':modul'            => $data['modul'],
            ]);
            header('Location: ' . URL . '/admin/pages');
        }

        $this->view->users = $users;
        $this->view->modul = $this->modul;

    }
    
    public function editAction()
    {
        $model = $this->model('Pages');
        $id = $this->param('param');
        $post = $model->getEditPost($id);
        $users = $model->getUsers();
        
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Zapisz')
        {
            $data = filter_input_array(INPUT_POST);
            if(empty($data['modul']))
            {
                $data['modul'] = null;
            }
            $model->updPosts([
                ':title'            => $data['title'],
                ':name'             => $data['name'],
                ':content'          => $data['content'],
                ':type'             => 'page',
                ':status'           => $data['status'],
                ':meta_title'       => $data['meta_title'],
                ':meta_keywords'    => $data['meta_keywords'],
                ':meta_description' => $data['meta_description'],
                ':author'           => $data['author'],
                ':modul'            => $data['modul'],
                ':id'               => $id,
            ]);
            header('Location: ' . URL . '/admin/pages');
        }
        

        $this->view->post  = $post;
        $this->view->users = $users;
        $this->view->modul = $this->modul;

    }
    
    public function deleteAction()
    {
        if($this->param('param'))
        {
            $id = $this->param('param');
            $model = $this->model('Pages');
            $model->delPosts($id);
            header('Location: ' . URL . '/admin/pages');
        }
    }
}
