<?php

namespace Admin\Controller;

use MVC\Controller\Controller;
/**
 * Description of Admin
 *
 * @author ptarkowski
 */
class AdminController extends Controller
{
    
    public function indexAction()
    {
        $msg = '';
        
//        return new ViewModel([
//            'msg' => $msg,
//        ]);
        $this->view->msg = $msg;
    }
    
    
    
    public function loginAction()
    {
        $msg = '';
        //echo '<pre>' . print_r($model->getAllUsersData(), 1) . '</pre>';
        
        //$login = $_POST['login'];
        //echo $session->is_hash('test');
        if(filter_input(INPUT_POST, 'logow') AND filter_input(INPUT_POST, 'logow') == 'Zaloguj')
        {
            $model = $this->model('Admin');
            
            $session = $this->session();
            $login = filter_input(INPUT_POST, 'login');
            $pass = hash('sha512', filter_input(INPUT_POST, 'passwd'));
            $result = $model->fetchByUser($login,$pass);
            
            //$session->access = 
            if(!empty($result)) 
            {
                
                //echo '<pre>' . print_r($roles,1) . '</pre>';
                if($result['is_active'] == 1)
                {
                    //$storage = Registry::getInstance();
//                    if($result['role_name'] == 'User') {
//                       $roles = $model->getRoles($result['id']);
//                        foreach($roles as $k => $v)
//                        {
//                            $rol[$v['grupa_id']] = $v['upr'];
//                        }
//                        $session->access = $rol;
//                    }    
                    $session->write($result);
                    
//                  echo '<br><br><br><pre>' . print_r($session->get('users'),1) . '</pre>';
//                  exit();
                    //echo json_encode(['status' => 0, 'message' => 'Dzieki ziom']);
                    //header('location: ' . URL);
                    echo json_encode(['status' => 1, 'message' => 'Pomyślnie zalogowano.']);
                    exit();
                    //
                } else {
                    echo json_encode(['status' => 0, 'message' => 'Konto jest nieaktywne.']);
                    exit();
                    //$msg = 'Bład logowania.';
                }
                
                
                
            } else {
                session_destroy();
                echo json_encode([ 'status' => 0, 'message' => 'Zły login i/lub hasło!' ]);
                //echo json_encode(array('status' => 0, 'message' => 'ups kurna nie działa'));
                
                //$msg = 'Błąd logowania';
                exit();
                //header("Location: " . URL . "/login");
            }
        }

        $this->view->message = $msg;
        $this->setTemplate('login');
    }
    
    public function registerAction()
    {
        $msg = '';
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Zarejestruj')
        {
            $token = md5(uniqid(rand()));
            
            $model = $this->model('Admin');
            $name = filter_input(INPUT_POST, 'login');
            
            if(filter_input(INPUT_POST, 'email',FILTER_VALIDATE_EMAIL))
            {
                $email = filter_input(INPUT_POST, 'email',FILTER_VALIDATE_EMAIL);
                $resEm = $model->getByMail($email, $name);
                if(empty($resEm))
                {
                if(!filter_input(INPUT_POST, 'password'))
                {
                    
                    $pass = $this->generatePass();
                } else {
                    $pass = filter_input(INPUT_POST, 'password');
                }
                
                $uid = $model->insUser([
                    ':login' => $name,
                    ':passwd' => hash('sha512', $pass),
                    ':token' => $token,
                    ':display_name' => filter_input(INPUT_POST, 'fname'),
                ]);
                
                $model->insUserData([
                    'fname' => filter_input(INPUT_POST, 'fname'),
                    'lname' => filter_input(INPUT_POST, 'lname'),
                    'uid' => $uid,
                    'email' => $email,
                    'role' => 3,
                ]);
                $link = URL . '/activated/user=' . $name . '&token=' . $token;
                $body = 'Witaj ' . $name . ', <br>Dziękujemy za zarejestrowanie konta. W tej chwili konto jest nieaktywne, aby aktywować kliknij w poniżwszy link.<br>';
                $body .= "<a href='" . $link . "'>" . $link . "</a><br><br>";
                $body .= "Twoje dane do konta to: <br>Nazwa użytkownika: " . $name . "<br>Hasło: " . $pass;
                $this->mail([
                    'body' => $body,
                    'to' => $email,
                    'subject' => 'Rejestracja nowego konta.',
                ]);
                $msg .= 'Dziekujemy za rejestrację, za chwile otrzymasz maila z dalszymi instrukcjami.';
                } else {
                    $msg .= 'Podany adres email lub/i login istnieje w bazie danych';
                }
            } else {
                $msg .= 'Podano niepoprawny adres email.';
            }
        
        

            
        }
        $this->view->msg = $msg;
        $this->setTemplate('login');
    }
    
    private function generatePass()
    {
        $ch = substr(str_shuffle('!@#$%&'), 0,1);
        $num = substr(str_shuffle('0123456789'), 0,1);
        $lch = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0,5);

        $hch = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 1);
        $pass = str_shuffle($ch.$num.$lch.$hch);
        return $pass;
    }
    
    
    private function mail($data)
    {
        
        $from = 'test@test.pl';// np:from ankieta@b2-biznes.pl

        $body = $data['body'];

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

        // Additional headers
        $headers .= 'To: Piotr ' . $data['to'] . "\r\n";
        $headers .= "From: " . $from  . "\r\n";
	
        $tresc = nl2br($body);
                    
                    //mail($to, $_POST['subject'], htmlspecialchars($tresc) );
        if(mail($data['to'], $data['subject'], $tresc, $headers ))
        {
            return true;
        }
    }
    
    public function lostAction()
    {
        
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Zdobądź nowe hasło')
        {
            $model = $this->model('Admin');
            if(filter_input(INPUT_POST,'login', FILTER_VALIDATE_EMAIL))
            {
                $email = filter_input(INPUT_POST,'login', FILTER_VALIDATE_EMAIL);
                $result = $model->getByUser($email);
            } else {
                $login = filter_input(INPUT_POST,'login');
                $result = $model->getByUser($login);
            }
            $body = '';
            $pass = $this->generatePass();
            $model->updPass([
                ':pass' => hash('sha512', $pass),
                ':login' => $result['login'],
            ]);
            $body .= 'Witaj ' . $result['login'] . ', <br>Twoje hasło zostało zmienione.<br>';
            $body .= "Twoje nowe hasło to: " . $pass;
            $this->mail([
                'body' => $body,
                'to' => $result['email'],
                'subject' => 'Zmiana hasła.'
            ]);
        }
        $this->view->setTemplate('login');
    }
    
    public function activatedAction()
    {
        //echo '<br><br><br>';
        //echo md5(rand());
        $model = $this->model('Admin');
        $msg = '';
        $token = $this->param('id');
        if(!empty($token)) {
            
        
            $token = explode('&', $token );
            $keys = explode('=', $token[0]);
            $user = array_combine((array)$keys[0], (array)$keys[1]);

            $value = explode('=', $token[1]);
            $token = array_combine((array)$value[0], (array)$value[1]);


            $data = array_merge($user, $token);

            //$token = array_combine((array)$token[0], (array)$token[1]);

            // echo md5(uniqid(rand()));
            //$token = md5(uniqid(rand()));
            $result = $model->selActivate($data);

            if(!empty($result))
            {
                if($result['is_active'] == '0')
                {
                    $model->updActivate([
                        'active' => 1,
                        'uid' => $result['id'],
                    ]);

                    $msg = 'Dziękujemy za rejestrację konto zostało aktywowane i możesz zalogować się.';
                    //exit();

                } else {
                    $msg = 'Konto jest już aktywne.';
                    //exit();
                }
            } else {
                $msg = 'Nie mamy takiego użytkownika i/lub zły token.';
            }
        
        }
        $this->view->msg = $msg;
        $this->view->setTemplate('login');
        //echo '<pre>' . print_r($result,1) . '</pre>';
    }
    
    
    
    public function db()
    {
        
        try {
            $dns = 'mysql:host=23185.m.tld.pl;dbname=baza23185_sport;charset=utf8';
            $db = new \PDO($dns, 'admin23185_sport', 'SP9876');
            return $db;
        } catch (\Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }
}
