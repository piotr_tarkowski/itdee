<?php
namespace Admin\Controller;

use MVC\Controller\Controller;
use View\ViewModel;

class BlockController extends Controller
{
    private $blocks = ['footer-one', 'footer-second', 'footer-third', 'footer-four'];
    
    public function indexAction()
    {
        $model = $this->model('Block');
        $bloki = $model->getBlocks();




        $this->view->blocks = $this->blocks;
        $this->view->block = $bloki;
        $this->view->bcontent = $model->getBlockContent();
        $this->view->bmenu = $model->getBlockMenu();

    }
    
    public function editAction()
    {
        $id = $this->param('param');
        $model = $this->model('Block');
        $data = $model->getEditContent($id);
        echo json_encode([ 'status' => 1, 'data' => $data ]);
        exit();
    }
    
    public function saveBlockAction()
    {
        $data = filter_input_array(INPUT_POST);
        echo '<br><br>';
        echo '<pre>' . print_r($data,1) . '</pre>';
        //exit();
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Zapisz')
        {
            $model = $this->model('Block');
            $id = $this->param('param');
            
            if($data['type'] == 'menu')
            {
                $model->updBlock([
                    ':type' => 'menu',
                    ':block_id' => null,
                    ':menu' => $data['menu'],
                    ':id' => $id,
                ]);
            }
            
            if($data['type'] == 'text')
            {
                $model->updBlock([
                    ':type' => 'text',
                    ':block_id' => $data['content'],
                    ':menu' => null,
                    ':id' => $id,
                ]);
            }
            if($data['type'] == 'clear')
            {
                $model->updBlock([
                    ':type' => 'clear',
                    ':block_id' => $data['content'],
                    ':menu' => null,
                    ':id' => $id,
                ]);
            }
            if($data['type'] == 'null')
            {
                $model->updBlock([
                    ':type' => null,
                    ':block_id' => null,
                    ':menu' => null,
                    ':id' => $id,
                ]);
            }
            header('Location: ' . URL . '/admin/block');
        }
    }
    
    public function saveTextAction()
    {
        
        if(filter_input(INPUT_POST, 'submit'))
        {
            $data = filter_input_array(INPUT_POST);
            $model = $this->model('Block');
            if(empty($data['showTitle']))
            {
                $data['showTitle'] = 0;
            }
            
            $model->updContent($data);
            header('Location: ' . URL . '/admin/block');
        }
    }
    
    public function newTextAction()
    {
        
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Dodaj')
        {
            $data = filter_input_array(INPUT_POST);
            $model = $this->model('Block');
            
            //echo '<pre>' . print_r($data,1) . '</pre>';
            if(empty($data['showTitle']))
            {
                $data['showTitle'] = 0;
            }
            $model->insBlock($data);
            
            header('Location: ' . URL . '/admin/block');
        }
    }
    
    public function deleteAction()
    {
        if(!empty($this->param('param')))
        {
            $model = $this->model('Block');
            $model->delContent($this->param('param'));
            header('Location: ' . URL . '/admin/block');
        }
    }
}
