<?php
namespace Admin\Controller;

use MVC\Controller\Controller;
use View\ViewModel;


class PostsController extends Controller
{
    private $modul =  [ 'contact' => 'Kontakt', 'test' => 'Test' ];
    
    public function indexAction()
    {
        $model = $this->model('posts');
        
        $posts = $model->getPosts();
        

        $this->view->posts = $posts;

    }
    
    public function newAction()
    {
        $model = $this->model('posts');
        $users = $model->getUsers();
        
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Dodaj')
        {
            //echo '<pre>' . print_r($_POST, 1) . '</pre>';
            $data = filter_input_array(INPUT_POST);
            
            $model->insPosts([
                ':title'            => $data['title'],
                ':name'             => $data['name'],
                ':content'          => $data['content'],
                ':type'             => 'post',
                ':status'           => $data['status'],
                ':meta_title'       => $data['meta_title'],
                ':meta_keywords'    => $data['meta_keywords'],
                ':meta_description' => $data['meta_description'],
                ':author'           => $data['author'],
            ]);
            header('Location: ' . URL . '/admin/posts');
        }


        $this->view->users = $users;

    }
    
    public function editAction()
    {
        $model = $this->model('posts');
        $id = $this->param('param');
        $post = $model->getEditPost($id);
        $users = $model->getUsers();
        
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Zapisz')
        {
            $data = filter_input_array(INPUT_POST);
            $model->updPosts([
                ':title'            => $data['title'],
                ':name'             => $data['name'],
                ':content'          => $data["content"],
                ':type'             => 'post',
                ':status'           => $data['status'],
                ':meta_title'       => $data['meta_title'],
                ':meta_keywords'    => $data['meta_keywords'],
                ':meta_description' => $data['meta_description'],
                ':author'           => $data['author'],
                ':id'               => $id,
            ]);
            header('Location: ' . URL . '/admin/posts');
        }
        

        $this->view->post  = $post;
        $this->view->users = $users;

    }
    
    public function uploadAction()
    {
        
        //$files = $_FILES;
        //$target = ROOTPATH . 'public/img/gal/' . $files['name'];
        reset ($_FILES);
        $temp = current($_FILES);
        
        
        $imageFolder = ROOTPATH . 'public/img/gal/';
        $filetowrite = $imageFolder . $temp['name'];
  
        move_uploaded_file($temp['tmp_name'], $filetowrite);
        
        // { location : '/your/uploaded/image/file'}
        //echo json_encode(array('location' => 'dobra/public/img/gal/'.$temp['name']));
        echo json_encode(array('location' => '/'.$temp['name']));
        exit();
        //move_uploaded_file($files['tmp_name'], $target );
        
    }
    
    public function imageListAction()
    {
        $imageFolder = ROOTPATH . 'public/img/upload/';
        $app = array_diff(scandir($imageFolder), array('.', '..'));
		
		echo json_encode($app);
		exit();
    }
	
	
    
    public function deleteAction()
    {
        if($this->param('param'))
        {
            $id = $this->param('param');
            $model = $this->model('posts');
            $model->delPosts($id);
            header('Location: ' . URL . '/admin/posts');
        }
    }
}
