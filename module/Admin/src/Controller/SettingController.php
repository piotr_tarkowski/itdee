<?php
namespace Admin\Controller;

use View\ViewModel;
use MVC\Controller\Controller;

class SettingController extends Controller
{
    public function indexAction()
    {
        $model = $this->model('Setting');
        $data = $model->getAll();
//        echo '<br><br>';
//        echo '<pre>' . print_r($admTpl,1) . '</pre>';
        
        foreach($data as $k => $v){
            $data[$v['option_name']] = $v['option_value']; 
        }
        
        $this->view->data = (object)$data;
    }
    
    public function edycjaAction()
    {
        
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Zapisz')
        {
            $data = filter_input_array(INPUT_POST);
            if(empty($data['guest_can_register']))
            {
                $data['guest_can_register'] = 0;
            }
            
            if(empty($data['route_page']))
            {
                $data['route_page'] = $data['styl_linkow'];
                unset($data['styl_linkow']);
            }
            
            unset($data['submit']);
            $model = $this->model('Setting');
            foreach($data as $k => $v)
            {
                $model->updSetting([
                    ':value' => $v,
                    ':name' => $k,
                ]);
            }
            
            header('Location: ' . URL . '/admin/setting');
            
        }
        
        
    }
    
    
}
