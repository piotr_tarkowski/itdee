<?php
namespace Admin\Controller;

use MVC\Controller\Controller;
use View\ViewModel;

class MenuController extends Controller
{
    
    
    public function editAction() 
    {
        
        if(filter_input(INPUT_POST, 'menu'))
        {
            $id = filter_input(INPUT_POST, 'menu');
            header('location: ' . URL . '/admin/menu/edit/' . $id);
        } elseif($this->param('param')) {
            $id = $this->param('param');
        } else {
            $id = 0; 
        }
        $model = $this->model('Menu');
        $links = $model->getAllLinks($id);
        $pages = $model->getPages('page');
        $blog  = $model->getPages('post');
        $menus = $model->getOneMenu($id);
        $men = $model->getAllMenu();
        $blocks = $model->getBlocks();
        

        $this->view->links = $links;
        $this->view->pages = $pages;
        $this->view->blog  = $blog;
        $this->view->menus = $menus;
        $this->view->blocks = $blocks;
        $this->view->men = $men;

    }
    
    
    public function newAction()
    {
        
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Utwórz menu')
        {
            $model = $this->model('Menu');
            
            $id = $model->insMenu([
                'name' => filter_input(INPUT_POST, 'name'),
            ]);
            header('location: ' . URL . '/admin/menu/edit/' . $id);
        }
        
    }
    
    public function updMenuAction()
    {
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Zapisz menu')
        {
            $model = $this->model('Menu');
            $id = $this->param('param');
            //echo '<br><br><br>';
            //echo '<pre>' . print_r($_POST, 1) . '</pre>';
            //exit();
            $data = filter_input_array(INPUT_POST);
            
            $model->updMenu([
                'name' => $data['menu_name'],
                'id' => $id
            ]);
            
            
            foreach($data['links'] as $k => $v)
            {
                if(is_array($v) && $v['type'] == 'custom_url'){
                    $model->updTitle([
                        ':title' => $v['title'],
                        ':url' => $v['url'],
                        ':target' => $v['target'],
                        ':id' => $k
                    ]);
                } else {
                    $model->updTitle([
                        ':title' => $v,
                        ':url' => null,
                        ':target' => null,
                        ':id' => $k
                    ]);
                }
                
            }
            
            
            if(!empty($data['block']))
            {
                
                foreach($data['block'] as $k => $v)
                {
                    $model->updBlock([
                        'menu' => $v,
                        'id' => $k
                    ]);
                }
            }
            
            header('location: ' . URL . '/admin/menu/edit/' . $id);
        }
    }
    
    public function addLinkAction()
    {
        $dad = filter_input_array(INPUT_POST);
        $id = $this->param('param');
        $model = $this->model('Menu');
        //echo '<pre>' . print_r($dad, 1) . '</pre>';
        if(filter_input(INPUT_POST, 'submit') && filter_input(INPUT_POST, 'submit') == 'Dodaj')
        {
            if(!empty($dad['pages']))
            {
                
                foreach($dad['pages'] as $k => $v)
                {
                    $fff = $model->getMaxLink($id);
                    $f = explode(';', $v);
                    $model->insLinkMenu([
                        'name' => $f[0],
                        'post_slug' => $f[1],
                        'order' => $fff['df']+1,
                        'post_id' => $k,
                        'type' => 'page',
                        'menu_id' => $id,
                    ]);
                }
            }
            if(!empty($dad['wlasny']['etykieta']))
            {
                if(empty($dad['wlasny']['target']))
                {
                    $dad['wlasny']['target'] = null;
                }
                $fff = $model->getMaxLink($id);
                $model->insLinkMenu([
                    'name' => $dad['wlasny']['etykieta'],
                    'order' => $fff['df']+1,
                    'url' => $dad['wlasny']['url'],
                    'target' => $dad['wlasny']['target'],
                    'type' => 'custom_url',
                    'menu_id' => $id,
                ]);
            }
            
        }
        header('location: ' . URL . '/admin/menu/edit/' . $id);
    }
    
    public function editingAction()
    {
        //echo '<pre>' . print_r($_POST, 1) . '</pre>';
        if(filter_input(INPUT_POST,'submit') && filter_input(INPUT_POST,'submit') == 'yes')
        {
            $model = $this->model('Menu');
            if(filter_input(INPUT_POST,'title'))
            {
                $model->updTitle([
                    ':title' => filter_input(INPUT_POST,'title'),
                    ':id' => filter_input(INPUT_POST,'id'),
                ]);
            }
            
            if(filter_input(INPUT_POST,'orders'))
            {
                $model->updOrders([
                    ':orders' => filter_input(INPUT_POST,'orders'),
                    ':id' => filter_input(INPUT_POST,'id'),
                ]);
            }
            
        }
    }
    
    public function delLinkAction()
    {
        $id = $this->param('param');
        $model = $this->model('Menu');
        $model->delLinks($id);
        exit();
    }
    
    public function deleteAction()
    {
        $id = $this->param('param');
        $model = $this->model('Menu');
        $model->updDelBlock($id);
        $model->delMenu($id);
        header('location: ' . URL . '/admin/menu/edit' );
    }
    

}
