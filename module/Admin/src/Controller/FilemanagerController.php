<?php
namespace Admin\Controller;

use MVC\Controller\Controller;
use View\ViewModel;


class FilemanagerController extends Controller
{
    
    
    public function indexAction()
    {
        
    }

    
    public function uploadAction()
    {
		
        $dir = ROOTPATH . 'public/img/upload/';
		$allow = [ 'jpg', 'jpeg', 'gif', 'png', 'tiff', 'bmp' ];


		if(!empty($_FILES['image'])){
			$ext = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);
					if(in_array($ext, $allow))
					{
						$image = time().'.'.$ext;
						move_uploaded_file($_FILES["image"]["tmp_name"], $dir.$image);
						$msg = "Poprawnie wgrano plik o nazwie " . $image;
						$status = 1;
					} else {
						$msg = "Zły format pliku. Możesz wgrać tylko: <STRONG>JPG, JPEG, GIF, PNG, TIFF, BMP</STRONG>";
						$status = 0;
						$image = '';
					}
					
		} else{
			$msg = "Plik jest pusty";
			$image = '';
			$status = 0;
		}
		//echo '<pre>' . print_r($_FILES, 1) . '</pre>';
		echo json_encode([ 'status' => $status, 'message' => $msg, 'file' => $image]);
		exit();
        
        
    }
    
    public function imageListAction()
    {
        $imageFolder = ROOTPATH . 'public/img/upload/';
        $app = array_diff(scandir($imageFolder), array('.', '..'));
		
		echo json_encode($app);
		exit();
    }
	
	
    
    public function deleteAction()
    {
        
		$dir = ROOTPATH . 'public/img/upload/';
		$tes = (array)json_decode(trim(file_get_contents('php://input')));
		$file = $tes['name'];
		//$file = filter_input(INPUT_POST,'code');
		//$dir = 'uploads/images/';
		
		$path = realpath($dir . $file);
		

		unlink($path);
		echo json_encode(['status' => 1, 'message' => 'Plik został usunięty']);
		exit();
    }
}
