<?php
namespace Admin\Controller;

use MVC\Controller\Controller;
use View\ViewModel;

class LayoutController extends Controller
{
    
    
    public function indexAction()
    {
        $admpath = ROOTPATH . 'module/Admin/view/template';
        $adm = array_diff(scandir($admpath), array('.', '..'));
        
        
        $apppath = ROOTPATH . 'module/Application/view/template';
        $app = array_diff(scandir($apppath), array('.', '..'));
        $model = $this->model('layout');
        $admEnable = $model->getEnable('admin_template');
        $pageEnable = $model->getEnable('page_template');
        

        $this->view->adm = $adm;
        $this->view->app = $app;
        $this->view->admEnable = $admEnable;
        $this->view->pageEnable = $pageEnable;

    }
    
    public function adminTemplateAction()
    {
        
        if(!empty($this->param('param')))
        {
            $admpath = ROOTPATH . 'module/Admin/view/template';
            $adm = array_diff(scandir($admpath), array('.', '..'));
            
            $temp = $adm[$this->param('param')];
            $model = $this->model('layout');
            $model->updAdminTemp($temp);
            header('Location: ' . URL . '/admin/layout');
            //echo '<pre>' . print_r($temp, 1) . '</pre>';
            //exit();
            
        }
        
        
    }
    
    public function pageTemplateAction()
    {
        
        if(!empty($this->param('param')))
        {
            $admpath = ROOTPATH . 'module/Application/view/template';
            $app = array_diff(scandir($admpath), array('.', '..'));
            
            $temp = $app[$this->param('param')];
            $model = $this->model('layout');
            $model->updPageTemp($temp);
            header('Location: ' . URL . '/admin/layout');
            //echo '<pre>' . print_r($temp, 1) . '</pre>';
            //exit();
            
        }
        
        
    }
    
}
