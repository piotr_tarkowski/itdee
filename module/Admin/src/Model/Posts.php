<?php
namespace Admin\Model;

use DB\Database;

class Posts 
{
    
    public function getPosts()
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select p.*, u.display_name as author from posts as p left join users as u on p.author=u.id where type=:type ');
        $stmt->execute([
            ':type' => 'post'
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getUsers()
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select * from users where is_active=:active');
        $stmt->execute([
            ':active' => 1
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getEditPost($id)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select * from posts where id=:id');
        $stmt->execute([
            ':id' => $id,
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function insPosts($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('insert into posts(title, name, content, type, status, meta_title, meta_keywords, meta_description, author) values(:title, :name, :content, :type, :status, :meta_title, :meta_keywords, :meta_description, :author)');
        $stmt->execute($data);
    }
    
    public function updPosts($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('update posts set title=:title, name=:name, content=:content, type=:type, status=:status, meta_title=:meta_title, meta_keywords=:meta_keywords, meta_description=:meta_description, author=:author where id=:id');
        $stmt->execute($data);
    }
    
    public function delPosts($id)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('delete from posts where id=:id');
        $stmt->execute([
            ':id' => $id,
        ]);
    }
    
    private function getDBCon()
    {
        $dbh = new Database();
        return $dbh->connection();
    }
}
