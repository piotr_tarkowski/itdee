<?php
namespace Admin\Model;

use DB\Database;

class Block 
{
    public function getBlocks()
    {
        $db = $this->getConDB();
        $stmt = $db->prepare('select b.*, c.*, b.id as bid, m.name as menu_name, b.name as block_name from block as b left join menu as m on b.menu_id=m.id left join `block-content` as c on b.content_id=c.id ');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getBlock($id)
    {
        $db = $this->getConDB();
        $stmt = $db->prepare('select b.*, c.*, b.id as bid, m.name as menu_name, b.name as block_name from block as b left join menu as m on b.menu_id=m.id left join `block-content` as c on b.content_id=c.id where b.id = :id');
        $stmt->execute([
            ":id" => $id,
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getEditContent($id)
    {
        $db = $this->getConDB();
        $stmt = $db->prepare('select * from `block-content` where id=:id');
        $stmt->execute([
            ':id' => $id,
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getBlockContent()
    {
        $db = $this->getConDB();
        $stmt = $db->prepare('select * from `block-content`');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getBlockMenu()
    {
        $db = $this->getConDB();
        $stmt = $db->prepare('select * from menu');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function updContent($data)
    {
        $db = $this->getConDB();
        $stmt = $db->prepare('update `block-content` set title=:title, showTitle=:show, text=:text where id=:id');
        $stmt->execute([
            ':title' => $data['title'],
            ':show' => $data['showTitle'],
            ':text' => nl2br($data['text']),
            ':id' => $data['id'],
        ]);
    }
    
    public function updBlock($data)
    {
        $db = $this->getConDB();
        $stmt = $db->prepare('update block set menu_id=:menu, content_id=:block_id, type=:type where id=:id');
        $stmt->execute($data);
    }
    
    public function insBlock($data)
    {
        $db = $this->getConDB();
        $stmt = $db->prepare('insert into `block-content` (title, text, showTitle) values (:title, :text, :show)');
        $stmt->execute([
            ':title' => $data['title'],
            ':text' => nl2br($data['text']),
            ':show' => $data['showTitle'],
        ]);
    }
    
    public function delContent($id)
    {
        $db = $this->getConDB();
        $stmt = $db->prepare('delete from `block-content` where id=:id');
        $stmt->execute([
            ':id' => $id
        ]);
    }
    
    private function getConDB()
    {
        $dbh = new Database();
        return $dbh->connection();
    }
}
