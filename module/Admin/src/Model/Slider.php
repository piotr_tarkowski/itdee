<?php
namespace Admin\Model;

use DB\Database;

class Slider 
{
    
    public function getSlider()
    {
        $dbh = $this->connDB();
        $stmt = $dbh->prepare('select * from slider order by ordering');
        $stmt->execute([
            ':active' => 1
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getMaxOrder()
    {
        $dbh = $this->connDB();
        $stmt = $dbh->prepare('select max(ordering) as ord from slider');
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function insSlider($data)
    {
        
        $dbh = $this->connDB();
        $stmt = $dbh->prepare('insert into slider (file, title, description, ordering) values (:file, :title, :desc, :order)');
        $stmt->execute([
            ':file'     => $data['file'],
            ':title'    => $data['title'],
            ':desc'     => $data['description'],
            ':order'    => $data['order']
        ]);
    }
    
    public function updSlider($data)
    {
        $dbh = $this->connDB();
        $stmt = $dbh->prepare('update slider set title= :title,description=:desc where id=:id');
        $stmt->execute([
            ':title'    => $data['title'],
            ':desc'     => $data['description'],
            ':id'       => $data['ids'],
        ]);
    }
    
    public function updImages($data)
    {
        $dbh = $this->connDB();
        $stmt = $dbh->prepare('update slider set file=:file where id=:id');
        $stmt->execute($data);
    }
    
    public function updOrder($data)
    {
        $dbh = $this->connDB();
        $stmt = $dbh->prepare('update slider set ordering=:orders where id=:id');
        $stmt->execute($data);
    }
    
    public function delSlider($id)
    {
        $dbh = $this->connDB();
        $stmt = $dbh->prepare('delete from slider where id=:id');
        $stmt->execute([
            ':id' => $id,
        ]);
    }
    
    private function connDB()
    {
        $dbh = new Database();
        return $dbh->connection();
    }
}
