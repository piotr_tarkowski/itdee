<?php
namespace Admin\Model;

use DB\Database;

class Admin
{
    public function getRole()
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select * from role');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function insUser($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('insert into users ( login, password, token, display_name ) values (:login, :passwd, :token, :display_name )');
        $stmt->execute($data);
        return $db->lastInsertId();
    }
    

    
    public function insUserData($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('insert into user_data (imie, nazwisko, email, users_id, role_id ) values (:fname, :lname, :email, :uid, :role)');
        $stmt->execute([
            ':fname' => $data['fname'],
            ':lname' => $data['lname'],
            ':email' => $data['email'],
            ':uid' => $data['uid'],
            ':role' => $data['role'],
        ]);
    }
    
    public function selActivate($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select * from users where login = :user and token = :token limit 1');
        $stmt->execute([
            ':token' => $data['token'],
            ':user' => $data['user'],
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function updActivate($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('update users set is_active = :active where id = :uid');
        $stmt->execute([
            ':active' => $data['active'],
            ':uid' => $data['uid'],
        ]);
    }
    
    public function updPass($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('update users set password = :pass where login = :login');
        $stmt->execute($data);
    }
    
    public function fetchByUser($login, $pass)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select u.*, ud.imie, r.role_name from users as u left join user_data as ud on u.id = ud.users_id left join roles as r on ud.role_id=r.id where login = :username and password = :password');
        $stmt->execute([
            ':username' => $login,
            ':password' => $pass,
        ]);
        
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getByUser($login)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select u.*,ud.email, ud.imie, r.role_name from users as u left join user_data as ud on u.id = ud.users_id left join roles as r on ud.role_id=r.id where u.login = :username or ud.email=:email');
        $stmt->execute([
            ':username' => $login,
            ':email' => $login
        ]);
        
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getByMail($email,$login)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select * from users as u inner join user_data as ud on u.id = ud.users_id  where u.login=:login or ud.email=:email');
        $stmt->execute([
            ':login' => $login,
            ':email' => $email,
        ]);
        
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    
    
    private function getDBCon()
    {
        $db = new Database();
        return $db->connection();
    }
}
