<?php
namespace Admin\Model;

use DB\Database;

class Layout 
{
    
    public function getEnable($name)
    {
        $db = $this->getDB();
        $stmt = $db->prepare('select option_value from setting where option_name=:temp');
        $stmt->bindParam(':temp', $name, \PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        
        return $result['option_value'];
    }
    
    public function updPageTemp($temp)
    {
        $db = $this->getDB();
        $stmt = $db->prepare("update setting set option_value=:temp where option_name='page_template'");
        $stmt->execute([
            ':temp' => $temp,
            
        ]);
    }
    
    public function updAdminTemp($temp)
    {
        $db = $this->getDB();
        $stmt = $db->prepare("update setting set option_value=:temp where option_name='admin_template'");
        $stmt->execute([
            ':temp' => $temp,
            
        ]);
    }
    
    private function getDB()
    {
        $dbh = new Database();
        return $dbh->connection();
    }
}
