<?php
namespace Admin\Model;

use DB\Database;

class Users 
{
    public function getAllUsers()
    {
        $db = $this->getDbCon();
        $stmt = $db->prepare('select u.*, ud.imie, ud.nazwisko, ud.email, r.role_name from users as u inner join user_data as ud on ud.users_id=u.id inner join roles as r on ud.role_id=r.id');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
        
    }
    
    public function getUser($id)
    {
        $db = $this->getDbCon();
        $stmt = $db->prepare('select u.*, u.id as uid, ud.imie, ud.nazwisko, ud.email, ud.role_id, ud.id as udid, r.role_name '
                . 'from users as u '
                . 'inner join user_data as ud on u.id=ud.users_id '
                . 'inner join roles as r on ud.role_id=r.id '
                . 'where u.id= :id ');
        $stmt->execute([
            ':id' => $id
        ]);
        $result = $stmt->fetch();
        return $result;
    }
    
    public function getUserByName($name)
    {
        $db = $this->getDbCon();
        $stmt = $db->prepare("select * from users where login LIKE CONCAT( :name, '%')");
        $stmt->execute([
            ':name' => $name
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getRoles()
    {
        $db = $this->getDbCon();
        $stmt = $db->prepare('select * from roles');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function insUser($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('insert into users ( login, password, is_active, token ) values (:login, :passwd, :active, :token )');
        $stmt->execute($data);
        return $db->lastInsertId();
    }
    
    public function insCompany($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('insert into firmy ( is_active ) values (:active)');
        $stmt->execute($data);
        return $db->lastInsertId();
    }
    
    
    
    public function insUserData($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('insert into user_data (imie, nazwisko, email, users_id, role_id ) values (:imie, :nazwisko, :email, :uid, :role)');
        $stmt->execute($data);
    }
    
    public function updUser($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare("update users set login = :login, is_active = :active where id = :id");
        $stmt->execute($data);
        return $db->lastInsertId();
    }
    
    public function updPass($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare("update users set password = :passwd where id = :id");
        $stmt->execute($data);
        return $db->lastInsertId();
    }
    
        
    public function updUserData($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('update user_data set imie = :imie, nazwisko = :nazwisko, email=:email, role_id=:role where id=:id');
        $stmt->execute($data);
    }
    
    public function delUser($id)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('delete from users where id=:id');
        $stmt->execute([':id' => $id]);
    }
    
    public function delUserDate($id)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('delete from user_data where users_id=:id');
        $stmt->execute([':id' => $id]);
    }
    
    private function getDbCon()
    {
        $db = new Database();
        return $db->connection();
    }
}
