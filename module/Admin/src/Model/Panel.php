<?php
namespace Admin\Model;

use DB\Database;

class Panel 
{
    public function getPanels()
    {
        $dbh = $this->getDB();
        $stmt = $dbh->prepare('select * from panel order by orders');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    public function getPanel($id)
    {
        $dbh = $this->getDB();
        $stmt = $dbh->prepare('select * from panel where id=:id');
        $stmt->execute([
            ':id' => $id
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    public function getMaxOrder()
    {
        $dbh = $this->getDB();
        $stmt = $dbh->prepare('select max(orders) as ord from panel');
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function insPanel($data)
    {
        $dbh = $this->getDB();
        $stmt = $dbh->prepare('insert into panel (title, text, image, orders, active) values (:title, :text, :image, :order, :active)');
        $stmt->execute([
            ':title'  => $data['title'],
            ':text'   => nl2br($data['text']),
            ':image'  => $data['image'],
            ':order'  => $data['order'],
            ':active' => 1,
        ]);
        
    }
    
    public function updImages($data)
    {
        $dbh = $this->getDB();
        $stmt = $dbh->prepare('update panel set image=:image where id=:id');
        $stmt->execute([
            ':image'  => $data['image'],
            ':id'  => $data['id'],
            
        ]);
    }
    
    public function updPanel($data)
    {
        $dbh = $this->getDB();
        $stmt = $dbh->prepare('update panel set title=:title, text=:text where id=:id');
        $stmt->execute([
            ':title'  => $data['title'],
            ':text'   => $data['text'],
            ':id'  => $data['id'],
            
        ]);
    }
    
    public function updOrder($data)
    {
        $dbh = $this->getDB();
        $stmt = $dbh->prepare('update panel set orders=:orders where id=:id');
        $stmt->execute($data);
    }
    
    public function delPanel($id)
    {
        $dbh = $this->getDB();
        $stmt = $dbh->prepare('delete from panel where id=:id');
        $stmt->execute([
            ':id'  => $id,
            
        ]);
    }
    
    private function getDB()
    {
        $db = new Database();
        return $db->connection();
    }
}
