<?php
namespace Admin\Model;

use DB\Database;

class Setting 
{
    public function getAll()
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select * from setting');
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function updSetting($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('update setting set option_value=:value where option_name = :name');
        $stmt->execute($data);
    }
    
    private function getDBCon()
    {
        $db = new Database();
        return $db->connection();
    }
}
