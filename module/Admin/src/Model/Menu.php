<?php
namespace Admin\Model;

use DB\Database;

class Menu 
{
    
    public function getAllLinks($menu)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select * from `menu-link` where menu_id=:menu order by ordering');
        $stmt->execute([
            ':menu' => $menu,
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getPages($type)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select * from posts where status=:status and type=:type');
        $stmt->execute([
            ':status' => 'publish',
            ':type' => $type
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    
    
    public function getBlocks()
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select * from block');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getAllMenu()
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select * from menu');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getOneMenu($id)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select * from menu where id=:id');
        $stmt->execute([
            ':id' => $id
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getMaxLink($id)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('select max(ordering) as df from `menu-link` where menu_id=:id');
        $stmt->execute([
            ':id' => $id,
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function insMenu($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('insert into menu (name) values (:name)');
        $stmt->execute([
            ':name' => $data['name']
        ]);
        $result = $db->lastInsertId();
        return $result;
    }
    
    public function insLinkMenu($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('insert into `menu-link` (name, post_slug,ordering, post_id, url, target, type, menu_id) values (:name, :post_slug, :order, :post_id, :url, :target, :type, :menu_id)');
        $stmt->execute([
            ':name' => $data['name'],
            ':post_slug' => $data['post_slug'],
            ':order' => $data['order'],
            ':post_id' => $data['post_id'],
            ':url' => $data['url'],
            ':target' => $data['target'],
            ':type' => $data['type'],
            ':menu_id' => $data['menu_id'],
        ]);
    }
    
    public function updMenu($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('update menu set name=:name where id=:id');
        $stmt->execute([
            ':name' => $data['name'],
            ':id' => $data['id'],
        ]);
    }
    
    public function updBlock($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('update block set type=:type, menu_id=:menu, content_id=:content where id=:id');
        $stmt->execute([
            ':type' => 'menu',
            ':menu' => $data['menu'],
            ':content' => null,
            ':id' => $data['id'],
        ]);
    }
    
    public function updOrders($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('update `menu-link` set ordering=:orders where id=:id');
        $stmt->execute($data);
    }
    
    public function updTitle($data)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('update `menu-link` set name=:title, url=:url, target=:target where id=:id');
        $stmt->execute($data);
    }
    
    public function delMenu($id)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('delete from menu where id=:id');
        $stmt->execute([
            ':id' => $id,
        ]);
    }
    
    public function delLinks($id)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('delete from `menu-link` where id=:id');
        $stmt->execute([
            ':id' => $id,
        ]);
    }
    
    public function updDelBlock($id)
    {
        $db = $this->getDBCon();
        $stmt = $db->prepare('update block set type=:type, menu_id=:menu, content_id=:content where id=:id');
        $stmt->execute([
            ':type' => null,
            ':menu' => null,
            ':content' => null,
            ':id' => $id,
        ]);
    }
    
    private function getDBCon()
    {
        $dbh = new Database();
        return $dbh->connection();
    }
}
