<?php
namespace Application\Controller;

use MVC\Controller\Controller;
use View\ViewModel;

class ContactController extends Controller
{
    
    
    public function indexAction()
    {
        
    }
    
    public function veryfiAction()
    {
        
        
        
//        echo '<pre>' . print_r($data, 1) . '</pre>';
//        exit();
        if(filter_input(INPUT_POST,'submit') === 'Wyślij')
        {
            
            $data = filter_input_array(INPUT_POST);
            //$this->sendMail($_POST);
                
            if($this->reCaptcha($data['g-recaptcha-response']) == 1)
            {
                //echo '<pre>' . print_r($_POST, 1) . '</pre>';
                
                $this->mail($data);
                header('Location: ' . URL . '/kontakt');
            } else {
                $msg = 'Potwierdź, że nie jesteś robotem!';
                header('Location: ' . URL . '/kontakt');
            }
            
            
            

        }
    }
    
    private function mail($data)
    {
        
        
        $to = 'kontakt@itdee.pl';
        //$to = 'noreply@itdee.pl';
        $from = $data['email'];
        $fname = $data['firstname'];
        $subject = $data['subject'];
        $body = $data['text'];
	
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

                    // Additional headers
                //$headers .= 'To: ' . $to . "\r\n";
        $headers .= 'From: ' . $fname . ' <' . $from . ">\r\n";
                
        //$tresc = '<b>FROM: </b><br />';
        //$tresc .= '<a href="mailto: ' . $from . '">' . $fname . '</a><br /><br />';
        //$tresc .= '<b>Treść wiadomości: </b>' . '<br />';
        $tresc = nl2br($body);
                   
                    //mail($to, $_POST['subject'], htmlspecialchars($tresc) );
        if(mail($to, $subject, $tresc, $headers ))
        {
            $msg = 'Dziękujemy za wysłanie wiadomości';
            
        }
        
    }
    
    private function reCaptcha($key)
    {
        
        $secretKey = "6LcbYhcUAAAAAHkxVbOvJMgO4AjCNzwbr1lII3qA";
        //$ip = $_SERVER['REMOTE_ADDR'];
        $ip = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        
        $response= file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$key."&remoteip=".$ip);
        $responseKeys = json_decode($response,true);
        //echo '<pre>' . print_r($responseKeys, 1) . '</pre>';
        //exit();
        //array_count_values($array);
        if(intval($responseKeys["success"]) !== 1) 
        {
            return false;
        } else {
            return true;
        }
    }
}
