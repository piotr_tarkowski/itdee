<?php
namespace Application\Controller;

use MVC\Controller\Controller;
use View\ViewModel;

class BlogController extends Controller
{


    public function indexAction()
    {
        $model = $this->model('blog');
        $limit = 2;
        if($this->param('param'))
        {
            $page = $this->param('param');
        } else {
            $page = 1;
        }
        $start_from = ($page-1) * $limit;
        $posts = $model->getTest(0, 1);

        
        return new ViewModel([
            'posts' => $posts
        ]);
    }
    
    public function wpisyAction()
    {
        $model = $this->model('blog');
        $limit = 10;
        if($this->param('param'))
        {
            $page = $this->param('param');
        } else {
            $page = 1;
        }
        
        $start = ($page-1) * $limit;
        $posts = $model->getTest($start, $limit);
        $ile = count($model->getPosts());
        
            
            
        
        
        return new ViewModel([
            'posts' => $posts,
            'ile' => ($ile / $limit) +1,
            'page' => $page,
        ]);
    }
    
    public function articleAction()
    {
        $model = $this->model('blog');

        if(!empty($this->param('param')))
        {
            $id = $this->param('param');
            $post = $model->getOnePost($id);
            
            if(!empty($post->meta_title))
            {
                $this->set('page_title', $post->meta_title);
            }
            if(!empty($post->meta_keywords))
            {
                $this->set('meta_keywords', $post->meta_keywords);
            }
            if(!empty($post->meta_description))
            {
                $this->set('meta_description', $post->meta_description);
            }
        }
        return new ViewModel([
            'post' => $post,
        ]);
    }
    
    public function paginationAction()
    {
        $limit = 2;  
        if (isset($_GET["page"])) { 
            $page  = $_GET["page"]; 
            
        } else { 
            $page=1; 
            
        }
        $start_from = ($page-1) * $limit;
        
        $model = $this->model('blog');
        $model->getTest();
    }
    
    
    
}
