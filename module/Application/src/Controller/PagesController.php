<?php
namespace Application\Controller;

use MVC\Controller\Controller;
use View\ViewModel;

class PagesController extends Controller
{
    

    public function indexAction()
    {
        
        $slug = $this->param('slug');
        $id = $this->param('id');
        
        $model = $this->model('Pages');
        $pages = $model->getPosts($slug, $id);
        
        if(!empty($pages->meta_title))
        {
            $this->set('page_title', $pages->meta_title);
        }
        if(!empty($pages->meta_keywords))
        {
            $this->set('meta_keywords', $pages->meta_keywords);
        }
        if(!empty($pages->meta_description))
        {
            $this->set('meta_description', $pages->meta_description);
        }
        if(!empty($pages->modul))
        {
            $this->set('viewModul', $pages->modul);
        }
        //$this->te();
        
        //echo $tes;
        //echo '<pre>' . print_r(filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT),1) . '</pre>';

//        return new ViewModel([
//            'slug' => $slug,
//            'id' => $id,
//            'post' => $pages,
//        ]);
        $this->view->slug = $slug;
        $this->view->id = $id;
        $this->view->post = $pages;
    }
    
    
    
    private function te()
    {

header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=document_name.doc");

echo "<html>";
echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
echo "<body style='font-size:12px;font-family:Arial'>";
echo "ąćęłńóśżź";
echo "</body>";
echo "</html>";

    }
}
