<?php
namespace Application\Controller;

use MVC\Controller\Controller;
use View\ViewModel;
use Auth\Session;


class HomeController extends Controller
{
    
    public function indexAction()
    {

        $this->set('front', true);
        $model = $this->model('Login');
        $slider = $model->getSlider();
        
        $panel = $model->getPanels();
        //$ll = $table->model('Login');
        $dd = $panel[0]['title'];
        //echo '<pre>' . print_r($tt->getUserData('admin'), 1) . '</pre>';
        //echo '<pre>' . print_r($_SESSION, 1) . '</pre>';
        //echo '<pre>' . print_r($_COOKIE, 1) . '</pre>';
        //echo $this->model()->fetch(1);
        
        //$session = new Session();
        //$session->destroy();
        //header('Refresh: 1');
        
//        return new ViewModel(array(
//            'panel' => $panel,
//            'slider' => $slider,
//        ));
        $this->view->panel = $panel;
        $this->view->slider = $slider;
        
    }
    
    public function loginAction()
    {
        $msg = '';
        
        //$session = new Session();
        //$session->destroy();
        //echo '<pre>' . print_r($_POST, 1) . '</pre>';
        
        if(filter_input(INPUT_POST, 'logowanie') AND filter_input(INPUT_POST, 'logowanie') === 'Zaloguj')
        {
            
            $session = new Session();
            $login = filter_input(INPUT_POST, 'login');
            $pass = $session->is_hash(filter_input(INPUT_POST, 'passwd'));
            $db = $this->getNewDBConnect();
            $db->select();
            $db->from('users');
            $db->where('username', '=', $login);
            $db->query($db);
            
            $result = $db->fetch();
            //echo '<pre>' . print_r($result, 1) . '</pre>';
            
            if($pass == $result['password'] AND $login == $result['login']) 
            {
                
                //$session->write($result);
                
                //$data['username'] = $login;
                //$data['role'] = $result['role'];
                //$registry = Registry::getInstance();
                //$registry->users = $result;
                
                //$session->logged = true;
                //$session->username = $login;
                //$session->display_name = $result['display_name'];
                //echo '<pre>' . print_r($result, 1) . '</pre>';
                $session->write($result);
                //exit();
                header('location: ' . URL );
            } else {
                $msg = 'Podano złe dane!';

                session_destroy();
                //header("Location: " . URL . "/login");
            }
            
        }
        $this->view->message = $msg;
    }
    
    
    
    public function contactAction()
    {
        $msg = '';
        
        if(isset($_POST['submit']) && $_POST['submit'] == 'Wyślij')
        {
            //echo '<pre>' . print_r($_POST, 1) . '</pre>';
            
            //$this->sendMail($_POST);
                
            if($this->reCaptcha($_POST['g-recaptcha-response']) == 1)
            {
                //echo '<pre>' . print_r($_POST, 1) . '</pre>';
                //$to = 'contact@itdee.pl';
                $to = 'ptarkowski@b2-biznes.pl';
                $from = $_POST['email'];
                $fname = $_POST['fname'];
                $subject = $_POST['subject'];
                $body = $_POST['tresc'];
		
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

                    // Additional headers
                //$headers .= 'To: ' . $to . "\r\n";
                $headers .= 'From: ' . $fname . ' <' . $from . ">\r\n";
                
                $tresc = '<b>FROM: </b><br />';
                $tresc .= '<a href="mailto: ' . $from . '">' . $fname . '</a><br /><br />';
                $tresc .= '<b>Tresc wiadomości: </b>' . '<br />';
                $tresc .= nl2br($body);
                   
                    //mail($to, $_POST['subject'], htmlspecialchars($tresc) );
                if(mail($to, $_POST['subject'], $tresc, $headers ))
                {
                    $msg = 'Dziękujemy za wysłanie wiadomości';
                }
                    
            } else {
                $msg = 'Potwierdź, że nie jesteś robotem!';
                   
            }
            
            
            

        }
        $this->view->message = $msg;
    }
    
    public function sendMail($data)
    {
        $to = $_POST['email'];//np.: ustawić "nadawcę" wiadomości np z $_POST['email'];
	$from = 'ankieta@b2-biznes.pl';// np:from ankieta@b2-biznes.pl 
	$fname = $_POST['fname'];
	$subject = $_POST['subject'];
	$body = $_POST['tresc'];
	unset($_POST['submit']);
        echo '<pre>' . print_r($_POST, 1) . '</pre>';
        exit();
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        
	// Additional headers
	$headers .= 'To: Piotr ' . $to . "\r\n";
	$headers .= 'From: ' . $fname . " <" . $from . ">" . "\r\n";
	$headers .= 'Bcc: ' . 'ankietawww@b2-biznes.pl' . "\r\n";// wstawić ukrytego odbiorcę np.: ankietawww@b2-biznes.pl
	$tresc = '<b>FROM: </b><br />';
        $tresc .= '<a href="mailto: ' . $from . '">' . $from . '</a><br /><br />';
        $tresc .= '<b>Treść wiadomości: </b>' . '<br />';
        $tresc .= nl2br($body);
                    
                    //mail($to, $_POST['subject'], htmlspecialchars($tresc) );
        if(mail($to, $_POST['subject'], $tresc, $headers ))
        {
            $msg = 'Dziękujemy za wysłanie wiadomości';
        }
    }
    
    
    public function logoutAction()
    {
        $sess = new Session();
        $sess->destroy();
        //header("Location: " . URL . "/login");
        header("Location: " . URL);
    }
    
    
    private function reCaptcha($key)
    {
        
        $secretKey = "6Lfo0QkUAAAAACO51Cx_HdujAD6eC3UWx4ET84a8";
        $ip = $_SERVER['REMOTE_ADDR'];
        
        $response= file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$key."&remoteip=".$ip);
        $responseKeys = json_decode($response,true);
        //echo '<pre>' . print_r($responseKeys, 1) . '</pre>';
        //exit();
        //array_count_values($array);
        if(intval($responseKeys["success"]) !== 1) 
        {
            return false;
        } else {
            return true;
        }
    }
    

    
    
    
    
}
