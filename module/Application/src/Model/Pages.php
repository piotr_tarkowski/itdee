<?php
namespace Application\Model;

use DB\Database;

class Pages 
{
    public function getPosts($slug, $id)
    {
        $dbh = $this->getDbCon();
        $stmt = $dbh->prepare('select * from posts where name = :slug or id=:id and status="publish"');
        $stmt->execute([
            ':slug' => $slug,
            ':id' => $id,
        ]);
        return $stmt->fetchObject();
    }
    
    public function getDbCon()
    {
        $dbh = new Database();
        return $dbh->connection();
    }
}
