<?php
namespace Application\Model;

use DB\Database;

class Blog 
{
    public function getPosts()
    {
        $dbh = $this->getDB();
        $stmt = $dbh->prepare('select p.*, u.display_name as author from posts as p left join users as u on p.author=u.id where type=:type and status=:status order by date_create');
        $stmt->execute([
            ':type' => 'post',
            ':status' => 'publish'
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getOnePost($id)
    {
        $dbh = $this->getDB();
        $stmt = $dbh->prepare('select p.*, u.display_name as author from posts as p left join users as u on p.author=u.id where type=:type and status=:status and p.id=:id');
        $stmt->execute([
            ':type' => 'post',
            ':status' => 'publish',
            ':id' => $id,
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getTest($start, $limit)
    {
        $post = 'post';
        $publ = 'publish';
        $dbh = $this->getDB();
        $stmt = $dbh->prepare("select p.*, u.display_name as author from posts as p left join users as u on p.author=u.id where type=:type and status=:status order by date_create LIMIT :start, :limit");
        $stmt->bindParam(':type', $post, \PDO::PARAM_STR);
        $stmt->bindParam(':status', $publ, \PDO::PARAM_STR);
        $stmt->bindParam(':start', $start, \PDO::PARAM_INT);
        $stmt->bindParam(':limit', $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    private function getDB()
    {
        $db = new Database();
        return $db->connection();
    }
}
