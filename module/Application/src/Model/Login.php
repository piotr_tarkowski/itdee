<?php
namespace Application\Model;
/**
 * Description of LoginTable
 *
 * @author ptarkowski
 */


use MVC\Registry;
use DB\Database;

class Login 
{
    public $table;
    private $registry;
    
    public function __construct() 
    {
        $this->registry = Registry::getInstance();
        //$this->table = new Login();
    }
    
    public function getPanels()
    {
        $db = $this->getConnDB()->connection();
        $stmt = $db->prepare('select * from panel order by orders');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    public function getUserData($user)
    {
        $db = $this->getConnDB()->connection();
        $stmt = $db->prepare('select u.*, ud.imie, ud.nazwisko, ud.email, r.role_name from users as u inner join user_data as ud on u.id=ud.users_id inner join roles as r on ud.role_id=r.id where login = :user');
        $stmt->execute([
            ':user' => $user,
        ]);
        $result = $stmt->fetchObject();
        
        return $result;
    }
    
    public function getUser($user)
    {
        $db = $this->getConnDB()->connection();
        $stmt = $db->prepare('select u.*, ud.imie, ud.nazwisko, ud.email, ud.data_dodania, r.role_name from users as u inner join user_data as ud on u.id=ud.users_id inner join roles as r on ud.role_id=r.id where login = :user');
        $stmt->execute([
            ':user' => $user,
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    public function getAllUsersData()
    {
        $db = $this->getConnDB()->connection();
        
        $stmt = $db->prepare('select * from users');
        //$stmt->bindValue(':user', $user, \PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    public function updUser($data)
    {
        $db = $this->getConnDB()->connection();
        $stmt = $db->prepare('update users set password=:password, display_name=:display_name where id = :id');
        $stmt->execute([
            ':password' => hash('sha512',$data['password']),
            ':display_name' => $data['display_name'],
            ':id' => $data['id'],
        ]);
    }
    
    public function updPass($data)
    {
        $db = $this->getConnDB()->connection();
        $stmt = $db->prepare('update users set password=:password where id = :id');
        $stmt->execute([
            ':password' => hash('sha512',$data['password']),
            ':id' => $data['id'],
        ]);
    }
    
    public function updUserData($data)
    {
        $db = $this->getConnDB()->connection();
        $stmt = $db->prepare('update user_data set imie=:fname, nazwisko= :lname, email= :email where users_id = :id');
        $stmt->execute([
            ':id' => $data['id'],
            ':fname' => $data['fname'],
            ':lname' => $data['lname'],
            ':email' => $data['email'],
        ]);
        
    }
    
    public function insert($data)
    {
        $db = $this->getConnDB();
        $stmt = $db->connection()->prepare('insert into users (login, password) values (:user, :pass) ');
        $stmt->bindValue(':user', $data['username']);
        $stmt->bindValue(':pass', $data['password']);
        $stmt->execute();
    }
    
    public function fetchByID($id)
    {
        $db = $this->getConnDB();
        $stmt = $db->connection()->prepare('select * from users where id = :id');
        $stmt->execute([
            ':id' => $name,
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function fetchByUser($login, $pass)
    {
        $db = $this->getConnDB();
        $stmt = $db->connection()->prepare('select u.*, ud.firma_id from users as u inner join user_data as ud on u.id=ud.users_id where login = :username and password = :password');
        $stmt->execute([
            ':username' => $login,
            ':password' => $pass,
        ]);
        
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getSlider()
    {
        $db = $this->getConnDB();
        $stmt = $db->connection()->prepare('select * from slider order by ordering');
        $stmt->execute([
            ':active' => 1, 
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function fetchAll()
    {
        $db = $this->getConnDB();
        $stmt = $db->connection()->prepare('select * from users');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getAllUserData()
    {
        
        $login = $_SESSION['username'];
        $db = $this->getConnDB();
        $stmt = $db->connection()->prepare('select * from users as u inner join user_data as ud on u.id=ud.users_id where login = :login');
        $stmt->execute([
            ':login' => $login,
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getMenu($pid)
    {
        $db = $this->getConnDB();
        $stmt = $db->connection()->prepare('select * from menu where parent = :par and active=1 order by pos_in_menu');
        $stmt->execute([
            ':par' => $pid,
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    private function getConnDB()
    {
        $db = new Database();
        return $db;
    }
}
