<?php
namespace Application\Error;

use MVC\Controller\Controller;
use View\ViewModel;
/**
 * Description of Error
 *
 * @author ptarkowski
 */
class Error extends Controller
{
    public function indexAction()
    {

        $error = $this->getAllParams();
        //var_dump($error);
        //$this->view->error = $error;
	return new ViewModel(array('error' => $error));
        //$view->setTemplate('error');
    }
}
