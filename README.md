## O projekcie

Projekt powstał w 2015 roku i jego założenie było stworzenie małego systemu CMS od postaw dla własnego użytku, ale też rozwijaniu swoich umiejętności. 

1. **Menu** - możemy dowolnie edytować nasz menu nawigacyjne, przypisywać je do rożnych "kontenerów", dodać tam istniejącą i aktywną stronę, post bloga, lub dodać własny link do strony zewnętrznej.
2. **Slider** - główny slider na stronie głównej gdzie możemy dodać tekst oraz wgrać obrazek z dysku. Możliwość jest też wybór obrazka, który jest już wgrany na nasz serwer.
3. **Panele** - zarządzanie 3 panelami, które 2 2 zdaniach opisze główne aspekty naszej działalności. Możemy tutaj też przypisać obraz.`<br>`
4. **Kontenery/Bloki** - głównie chodzi o naszą stopkę gdzie możemy dodawać w niej przeróżne rzeczy np. Linki do stron społecznościowych, kontakt, menu nawigacyjne, czy też po prostu zwykły tekst.
5. **Motywy** - możliwość zmiany  motywu/layoutu naszej strony
6. **Ustawienia** - Edycja podstawowych danych strony, ustawienie głównego tytułu stron, czy też znaczników meta. Tutaj ustawimy także wygląd naszych linków.
7. **Użytkownicy** - Dodawanie/usuwanie/edycja użytkowników

---
## Wymagania
1. PHP 5.6
2. Baza danych

## Instalacja

Aby prawidłowo zainstalować/uruchomić aplikację trzeba wykonać kilka kroków.

1. Plik public/.htaccess.dist zmienić na public/.htaccess
2. Zmienić nazwę pliku config/local.config.php.dist na config/local.config.php
3. Utworzyć bazę danych i zaimportować do niej plik /data/database.sql
4. W pliku local.config.php zmienić dane do połączenia z naszą bazą danych:
	- DATABASE_NAME - nasza nazwa bazy danych
	- DB_USERNAME - nasz użytkownik do połączenia z bazą danych 
	- DB_PASSWORD - hasło do bazy danych
	- jeśli adres do połączenia z bazą danych jest inny niż localhost to też trzeba zmienić na inny
5. Zalogować można się na stronie domena.ltd/login danymi login: 
	- **login:** admin,
	- **hasło:** admin354
