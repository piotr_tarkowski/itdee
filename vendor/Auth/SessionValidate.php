<?php
namespace Auth;

/**
 * Description of SessionValidate
 *
 * @author ptarkowski
 */

use Auth\Browser;

class SessionValidate 
{
    private $sessUser;
    private $sessId;
    private $sessTime;
    private $sessRole;
    private $sessBrowser;
    private $sessIp;
    
    private $acl;
    public $allow;
    
    //public $role;
    
    
    public function __construct($data) 
    {
        $this->setData($data);
        $this->getConfACL();
    }
    
    
    
    private function setData($data)
    {
        if(!empty($data))
        {
            $this->sessUser = $data['session_username'];
            $this->sessId = $data['session_id'];
            $this->sessTime = $data['session_time'];
            $this->sessIp = $data['session_ip'];
            $this->sessRole = $data['role'];
            $this->sessBrowser = $data['browser'];
        } else {
            $this->sessRole = 'guest';
        }
        
    }
    
    public function checkCookie()
    {
        if(isset($_COOKIE['sessionid']) && $_COOKIE['sessionid'] == $this->sessId)
        {
            $this->checkBrowser();
        }
    }
    
    public function checkUserIP()
    {
        if($this->sessIp == $this->getUserIP())
        {
            $this->checkUsername();
        }
    }
    
    public function checkUsername()
    {
        if($this->sessUser == $_SESSION['username'])
        {
            
            return $this->getPersmission();
        }
    }
    
    
    
    private function getPersmission()
    {
        return $this->allow;
    }
    
    private function setPersmission($routeName)
    {
        if(in_array($routeName, $this->acl[$this->role]))
        {
            //$this->allow = $this->acl[$this->role];
            if($this->role != 'guest')
            {
                $this->acl[$this->role] = array_merge($this->acl[$this->role], $this->acl['guest'] );
            }
            
            return $this->acl[$this->role];
        }
    }
    
    public function getUserIP()
    {
        if(isset($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }
    
    private function getConfACL()
    {
        $this->acl = require(ROOTPATH . 'config/acl.config.php');
        return $this;
    }
}
