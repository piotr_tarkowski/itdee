<?php
namespace Auth;

/**
 * Description of Session
 *
 * @author ptarkowski
 */

use DB\Database;
use Auth\Browser;
use MVC\Registry;

class Session 
{
    private $algo = 'sha512';
    
    public function start()
    {
        //session_start();
        //$time = time();
        //$_SESSION['lstatus'] = true;
        
    }
    
    
    public function session_id($user)
    {
        
        $hash = hash($this->algo, uniqid() . $user);
        $this->sessionid = $hash;
        return $hash;
    }
    
    public function is_hash($data)
    {
        $hash = hash($this->algo, $data);
        return $hash;
    }
    
    public function clear()
    {
        $db = $this->getSessData();
        $stmt = $db->connection()->prepare('delete from session where session_time < :time');
        //$stmt->bindValue();
        $stmt->execute(array(
            ':time' => time()
        ));
    }
    
    public function read()
    {
        //$sess_id = !empty($_SESSION['session_id']) ? $_SESSION['session_id'] : '';
        if(!isset($SESSION))
        {
            session_start();
        }
        
        $db = $this->getSessData();
        $stmt = $db->connection()->prepare('select * from session where session_id = :sessid');
        $stmt->bindValue(':sessid', $this->sessionid);
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        
//        $db->select();
//        $db->from('session');
//        $db->where('session_id', '=', $this->sessionid);
//        $db->query($db);
//        $result = $db->fetch();
        return $result;
        
    }
    
    public function write($data)
    {
        //$this->clear();
        $username = $data['login'];
        $role = str_replace(' ', '_', strtolower($data['role_name']));
        //$db = $this->getSessData();
        //echo '<pre>' . print_r($data,1) . '</pre>';
        //exit();
        
        $DateTime = date('Y-m-d H:i:s');
        //session_name();
        //$expTime = date('Y-m-d H:i:s',strtotime($DateTime.' + 1 hour'));
        $extimes = time() + 86400; //3600 = 1 hour
        //$extimes = time() + 604800; <- 7 dni
        $ipuser = $this->getUserIP();
        $sessid = hash( 'sha512', session_id() . $username . time() . URL);
        
        setcookie('sessionid', $sessid, $extimes, '/');
        $this->sessionid = $sessid;
        $this->username = $username;
        $this->role = $role;
        
        
        
        
        //$registry = Registry::getInstance();
        
        
        $db = $this->getSessData();
//        $stmt = $db->connection()->prepare('select * from session where session_username = :user');
//        $stmt->bindValue(':user', $username);
//        $stmt->execute();
//        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        //$sessionname = 'admin';
        $displayname = $data['imie'];
        if(empty($data['imie'])) {
            $displayname = ucfirst($data['login']);
        } 
        $this->displayName = $displayname;
        $this->logged = true;
        
        $browser = $this->getBrowser();
        
        
        
        //echo '<pre>' . print_r($browser,1) . '</pre>';

//        if(!empty($result))
//        {
//            $this->updData($db, array(
//                'sess_id'   => $sessid,
//                'username'  => $username,
//                'sess_ip'   => $ipuser,
//                'time'      => $extimes,
//                'role'      => $role,
//                'browser'   => $browser->Name,
//            ));
//        } else {
            $this->insData($db, array(
                'sess_id'   => $sessid,
                'username'  => $username,
                'sess_ip'   => $ipuser,
                'time'      => $extimes,
                'role'      => $role,
                'browser'   => $browser->Name,
            ));
        //}
        //$storage = Registry::getInstance();
        //echo '<pre>' . print_r($storage,1) . '</pre>';
        
    }
    
   
    
    public function getBrowser()
    {
        $browser = new Browser();
        return $browser;
    }
    
    private function updData($db, $data)
    {

        $stmt = $db->connection()->prepare('update session set session_id = :sess_id, session_username = :user, role = :role, session_time = :time, session_ip = :sess_ip, browser = :browser');
        $stmt->execute(array(
            ':sess_id'  => $data['sess_id'],
            ':user'     => $data['username'],
            ':sess_ip'  => $data['sess_ip'],
            ':time'     => $data['time'],
            ':role'     => $data['role'],
            ':browser'  => $data['browser'],
        ));
    }
    
    private function insData($db, $data)
    {

        $stmt = $db->connection()->prepare('insert into session (session_id,session_username,session_ip,session_time,role,browser) values (:sess_id, :user, :sess_ip, :time, :role, :browser)');
        $stmt->execute(array(
            ':sess_id'  => $data['sess_id'],
            ':user'     => $data['username'],
            ':sess_ip'  => $data['sess_ip'],
            ':time'     => $data['time'],
            ':role'     => $data['role'],
            ':browser'  => $data['browser'],
        ));
    }
    
    public function destroy()
    {
        if (!isset($_SESSION))
        {
            session_start();
        }
        $db = $this->getSessData(); //getDB()->prepare('delete from session where session_id= :sess_id');
        $stmt = $db->connection()->prepare('delete from session where session_id = :sessid');
        //$stmt->bindValue(':user', $user);
        $stmt->bindValue(':sessid', $this->sessionid);
        $stmt->execute();
        
        
        
        $this->logged = false;
        $inKey = 'sessionid';
        //clearpieces( $inKey , 1 ); 
        
        setcookie( $inKey , '' , time()-3600 , '/' , '' , 0 ); 
        unset($_COOKIE[$inKey]); 
        session_unset();
        session_destroy();
        //header("header: /");
    }
    
    public function getUserIP()
    {
        if(isset($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }
    
    private function getSessData()
    {
        $db = new Database();
        
        return $db;
    }
    
    public function get($name)
    {
        if(!empty($name))
        {
            $registry = Registry::getInstance();
            return $registry->$name;
        }
    }
    
    public function set($name, $value)
    {
        $registry = Registry::getInstance();
        
        return $registry->$name = $value;
        
        
    }
    
    public function __set($name,$value)
    {
        $_SESSION[$name] = $value;
    }
    
    public function __get($name) 
    {
        if(isset($_SESSION[$name])) 
        {
            return $_SESSION[$name];
        }
        
    }
    
    
    
}
