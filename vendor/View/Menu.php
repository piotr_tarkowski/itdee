<?php
namespace View;

use DB\Database;


class Menu 
{
    private $menu;
    private $position;
    private $block;
    
    public function __construct($pos) {
        $this->block = $this->getBlock($pos);
        
        $this->position = $pos;
    }
    
    public function getBlock($pos)
    {
        $db = $this->getDataDB();
        $stmt = $db->connection()->prepare('select b.*, bc.* from block as b left join menu as m on b.menu_id=m.id left join `block-content` as bc on b.content_id=bc.id where b.position=:pos');
        
        $stmt->execute([
            ':pos' => $pos,
        ]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);      
        return $result;
    }
    
    public function getSubMenu($idmenu)
    {
        $db = $this->getDataDB();
        $stmt = $db->connection()->prepare('select * from submenu where menu_id = :idmenu');
        $stmt->execute(array(
            ':idmenu' => $idmenu,
        ));
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getMenu()
    {
        $db = $this->getDataDB();
        $stmt = $db->connection()->prepare('select * from `menu-link` where menu_id=:id order by `ordering`');
        
        $stmt->execute([
            ':id' => $this->block['menu_id'],
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);      
        return $result;
    }
    
    public function getSMenu()
    {
        $db = $this->getDataDB();
        $stmt = $db->connection()->prepare('select * from menu-link where parent > 0 and active=1 order by pos_in_menu');
        
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);      
        return $result;
    }
    
    private function getDataDB()
    {
        $db = new Database();
        return $db;
    }
    
    public function renderMenu()
    {
        
    }
    
    public function tester()
    {
        echo ' test k ... ';
    }
}
