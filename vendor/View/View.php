<?php
namespace View;

use MVC\Registry;
use View\Menu;
use DB\Database;
use View\Resources\ViewData;

class View 
{
    
    //private $_registry;
    //private $menu;
    public $datamodul = null;


    public function __construct() 
    {
        //$this->_registry = Registry::getInstance();
        //echo '<pre>' . print_r($this->_registry,1) .'</pre>';
        
    }
    
    
    public function menu($pos)
    {
        $menu = new Menu($pos);
        return $menu;
    }
    
    public function css($data)
    {
        $css = '<link rel="stylesheet" ';
        $path = '/css/';
        if(isset($data['href']))
        {
            $css .= 'href="' . URL . $path . $data['href'] . '" >';
        }
        return $css;
    }
    
    
    
    public function js($data)
    {
        $js = '<script ';
        $path = '/js/';
        if(isset($data['src']))
        {
            $js .= 'src="' . URL . $path . $data['src'] . '" ';
            $js .= 'type="text/javascript" >';
        }
        $js .= '</script>';
        return $js;
        
    }
    
    
    
    public function blocks($block, $ch = null)
    {
        $bloki = $this->menu($block)->getBlock($block);
        
        if(!empty($bloki['type'])){
            $path = TEMPLATE_PATH . $this->get('setting')['page_template'] . '/blocks/' . $bloki['type'] .'.phtml';
            $this->menus = $this->menu($block)->getMenu(0);
            if(!empty($ch))
            {
                return true;
            }
            if(!empty($bloki['content_id']))
            {
                $this->blocks = array();
                $this->blocks['showTitle'] = $bloki['showTitle'];
                $this->blocks['title'] = $bloki['title'];
                $this->blocks['text'] = $bloki['text'];
            }

            if(file_exists($path))
            {
                include $path;
                
                
            }
        }
        
        
        //echo '<pre>' . print_r($bloki, 1) . '</pre>';
        
        
        /*<!--<ul class="block-menu">
            <li>
                <a href="<?=URL;?>">Home</a>
            </li>
            <?php foreach ($this->menu('footer-one')->getMenu(0) as $key => $value): ?>
            <li>
                <a href="<?php if(empty($value['url'])){ echo URL . $this->url('pages', ['slug' => $value['post_slug'], 'action' => $value['post_id']]); } else { echo $value['url']; } ?>" <?php if(!empty($value['url'])){ ?>target="_blank" <?php } ?> id="<?= $value["id"]; ?>" ><?= $value['name']; ?></a>
            </li>
            <?php endforeach; ?>            
        </ul>-->*/
        
                    
    }
    
    public function url($name, $options = null)
    {
        $tt = $this->get('route');
        $route = (object)$tt[$name];
        //echo array_count_values();
        //echo '<pre>' . print_r($route,1) . '</pre>';
        //echo $url;
 //echo '<pre>' . print_r($options,1) . '</pre>';
        if(count($route->params) > 0)
        {
            //$url = ltrim($route->route,'/');
            $url = $route->route;
            $url = str_replace(['[',']',':'], ['','',''], $url);
            $nurl = substr($url, 1, strpos(ltrim($url,'/'), '/'));
            
        } else {
            $nurl = $route->route;
        }
        
        
        if(!empty($options))
        {
            
            $ty = explode('/', $url);

            $zz = array_diff($ty, array_keys($route->params));

            $uuu = explode('/', $route->route);
            $par = $route->params;
            //foreach(array_keys($route->params) as $k=>$v)
            foreach(array_keys($options) as $k=>$v)
            {
                if(in_array($v, array_keys($uuu)) )
                {
                    $tz[$v] = $options[$v];
                } else {
                    unset($par[$v]);
                    unset($tz[$v]);
                }
            }
            //echo '<pre>' . print_r($tz,1) . '</pre>';
//            if(empty($tz['id']))
//            {
//                unset($tz['id']);
//            }
//            
//            if(empty($tz['action']))
//            {
//                unset($tz['action']);
//            }
            //$curl = str_replace($zz[0], '', $url);
            
            if(preg_match('/\-/', $url))
            {
                $nurl = str_replace(array_keys($par), $tz, $url);
                
                //echo '<pre>' . print_r($par, 1) . '</pre>';
                
            } else {
                $yy = str_replace(array_keys($par), $tz, $url);
                //echo '<pre>' . print_r($yy, 1) . '</pre>';
                $yu = explode('/',$yy);
                
                foreach($yu as $l => $p)
                {
                    if(empty($p))
                    {
                        unset($yu[$l]);
                    }
                }
                //$nurl = str_replace( array_keys($par), array_values($tz), $url );
                //echo '<pre>' . print_r($yu, 1) . '</pre>';
                //$tz = array_merge($zz,array_values($tz));
                //$yy = array_diff($tz, );
                $nurl = $yy;
                $nurl = implode('/', $yu);
            }
            
        } 
        //echo '<pre>' . print_r($url, 1) . '</pre>';
        $nurl = '/' . ltrim($nurl, '/');

        return  $nurl;
        
    }
    
    public function test($name, $param)
    {
        //echo '<pre>' . print_r(func_get_args(), 1) . '</pre>';
        
        
        
        switch ($name)
        {
            case 'get':
                return call_user_func([$this, $name], $param);
                //break;
            case 'helper':
        }
        
    }
    
    
    
    public function isAllowed($name)
    {
        //$role = isset($_SESSION['role']) ? $_SESSION['role'] : '';
        
        if(isset($_SESSION['logged']))
        {
            //$this->allow = (object)$_SESSION;
            
            $db = new Database();
            $stmt = $db->connection()->prepare('select * from session where session_id = :sesid');
            $stmt->execute([
                ':sesid' => $_SESSION['sessionid'],
            ]);
            $res = $stmt->fetch(\PDO::FETCH_ASSOC);
            
            if(!empty($res))
            {
                
                $stm = $db->connection()->prepare('select u.*, ud.imie, ud.email, r.role_name from users as u left join user_data as ud on u.id=ud.users_id inner join roles as r on ud.role_id=r.id where login = :user');
                $stm->execute([
                    ':user' => $res['session_username'],
                    
                ]);
                $result = $stm->fetch(\PDO::FETCH_ASSOC);
                
                
                
            }
            //echo '<pre>' . print_r($result, 1) . '</pre>';
            switch ($name) {
                case 'author':
                    return $result['id'];
                case 'displayName':
                    
                    if(!empty($result['display_name'])){
                        return $result['display_name'];
                    } else {
                        return $result['login'];
                    }
                    //break;
                case 'logged':
                    if(isset($_SESSION[$name]))
                    {
                        return true;
                    } else {
                        return false;
                    }
                    break;
                case 'role':
                    
                    return strtolower($result['role_name']);
                    //break;
                
                    //break;
            }
            
            
            //return $_SESSION[$name];
            
                
            
            //return true;
            
            //return $result[$name];
        }
        
    }
    
    public function loadView($file, $data)
    {
        //echo $file;
        //$data = (object) $data;
        if(is_readable($file))
        {
            if(!empty($data))
            {
               extract((array)$data);
            }
            $this;
            include $file;
        }
    }
    
    public function setting()
    {
        return $this->get('setting');
    }
    
    public function render($layout, $vars, $loadContent, $position = null)
    {
        //var_dump($vars);exit;
        //$this->access();
        $setting = $this->get('setting');
        $this->meta_title = $setting['meta_title'];
        $this->pagefooter = $setting['pagefooter'];
        if(!empty($this->get('page_title')))
        {
            $this->page_title = $this->get('page_title');
        }
        if(empty($this->get('meta_keywords')))
        {
            $this->meta_keywords = $setting['meta_keywords'];
        } else {
            $this->meta_keywords = $this->get('meta_keywords');
        }
        $this->page_slang = $setting['page_slang'];
        
        if(empty($this->get('meta_description'))){
            $this->meta_description = $setting['meta_description'];
        } else {
            $this->meta_description = $this->get('meta_description');
        }
        
        
        $ext = '.phtml';
        $defaultName = 'layout' . $ext;
        
        $modules = $this->get('modules');
        $mod = $this->get('config');
        //echo '<br><br>';
        //echo '<pre>' . print_r($name,1) . '</pre>';
        if($this->get('template'))
        {
            $defaultName = $this->get('template') . $ext;
        }
        if($_GET['module'] !== 'Admin')
        {
            $module = $modules['Application'];
            $template = $module . '' . $layout . $setting['page_template'] . '/';
            $name = $template . $defaultName;
        } else {
            $module = $modules['Admin'];
            $template = $module . '' . $layout . $setting['admin_template'] . '/';
            $name = $template . $defaultName;
        }

            //echo $template;
        $name = str_replace('\\', '/', $name);
            if(file_exists(ROOTPATH . $name ))
            {
                
                //echo $file = ROOTPATH . 'module' . DIRECTORY_SEPARATOR . $module . $name;
                $file = ROOTPATH . $name;
            }
		$template = str_replace('\\', '/', ROOTPATH . $template);
        $position = ['header', 'content', 'footer'];
        
        
        if(!empty($this->get('viewModul')) && file_exists($template . 'modul/' . $this->get('viewModul') . '.phtml'))
        {
			$dat = new ViewData();
            $this->datamodul = $dat->getData($this->get('viewModul'));
            $this->setBox($this->get('viewModul'), $template . 'modul/');
            $this->viewModul = ob_get_clean();
        }
        if(!empty($this->get('front')))
        {
            $this->front = true;
        }
        
        
        $this->setBox('header', $template);
        ${$position[0]} = ob_get_clean();
        
        ob_start();
        $this->loadView($loadContent, $vars);
        $this->content = ob_get_clean();
        
        $this->setBox('content', $template);
        ${$position[1]} = ob_get_clean();
        
        
        $this->setBox('footer', $template);
        ${$position[2]} = ob_get_clean();
        
        
        //echo $file;
        //if(!empty($file)){
            include($file);
        //}
        
    }
    
    public function setBox($name, $module)
    {
        //echo $module;
        if(!empty($name))
        {
            ob_start();
            include( $module . $name . '.phtml');
        }
        
    }
    

    
    public function set($name, $value)
    {
        $registry = Registry::getInstance();
        $registry->$name = $value;
    }
    
    public function get($name) 
    {
        
        if(!empty($name))
        {
            $registry = Registry::getInstance();
            return $registry->$name;
        }
        return false;
    }
    
    public function __set($name, $value)
    {
        
        $this->$name = $value;
    }
    
    public function __get($name) 
    {
        if(isset($name))
        {
            
            return $this->$name;
        }
        return false;
    }
}
