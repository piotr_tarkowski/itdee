<?php
namespace View\Resources;

use DB\Database;

class ViewData
{
    
    public function getData($data)
    {
        $db = new Database();
        $stmt = $db->connection()->prepare('select data from module where name=:name and active=:act');
        $stmt->bindValue(':name', $data, \PDO::PARAM_STR);
        $stmt->bindValue(':act', 1, \PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        
        //echo $result;
        //echo var_dump($result);
        $result = unserialize($result['data']);
        return $result;
    }
}
