<?php
namespace View;


use MVC\Registry;

class ViewModel {
    
    private $registry;
    private $variables;
    private static $_instance;

    public static function getInstance()
    {
        if(!self::$_instance instanceof self) {
            self::$_instance = new ViewModel();

        }

        return self::$_instance;
    }

    public function __construct($vars = null) 
    {
        $this->registry = Registry::getInstance();
        //$this->setVariables('variables',$vars);
    }
    
    private function setVariables($name,$value)
    {
        if(!empty($value))
        {
            $this->registry->$name = $value;
        }
        
    }
    
    public function setTemplate($name)
    {
        if(!empty($name))
        {
            $this->registry->template = $name ;
        }
        
    }
    
    public function setModul($name)
    {
        if(!empty($name))
        {
            $this->registry->viewModul = $name ;
        }
    }

    public function setVariable($value)
    {
        $registry = Registry::getInstance();
        $registry->variables = $value;

    }
    

}