<?php
namespace Router;

use MVC\Registry;

class Router
{
    private $route = array();
    private $class;
    private $action;
    private $params = array();

    
    protected static $collection;
    protected $url;
    protected $file;
    protected $name_route;
    
    public function __construct($url, $collection)
    {
        $this->url = $url;
        $this->setCollection($collection);
        
    }
    
    public function setNameRoute($name)
    {
        $this->name_route = $name;
    }
    
    public function getNameRoute()
    {
        return $this->name_route;
    }
    
    public function get($name)
    {
        $registry = Registry::getInstance();
        return $registry->$name;
    }
    
    public function set($name, $value)
    {
        $registry = Registry::getInstance();
        $registry->$name = $value;
    }
    
    
    
    public function run()
    {
        foreach($this->getCollection()->getAll() as $route)
        {
            if($this->matchData($route))
            {
                $this->setData($route);
                return true;
            }
            
        }

        
        throw new \Exception($_SERVER['REQUEST_URI'].' Nie mozna powiazać adresu z routingiem.', 404);
    }
    
    
    public function matchData($route)
    {
        //echo $route->getRoute();
        
        $params = array();
        $keyParams = array_keys($route->getParams());
        $keys = $route->getParams();
        $url = $route->getRoute();
        //
        //    (?:-(?P\d+))?
       // echo preg_match('#(<(.*):(.*)>)+#', $url, $mth);
        //if(){
            //$url = str_replace('[/:','',$url);
            //$params = str_replace('[/:','',$url);
            
            //print_r($mth);
            //echo 'test jest ok';
        //}
        
        foreach($keyParams as $k=>$key)
        {
            
            if(preg_match("((?:\[/\:)+)", $url))
            {
                //$regex .= '(?:/' . $yy . ')?';
                $params['[/:' . $key . ']'] = '(?:/(?P<' . $key . '>'. $keys[$key] .'))?';
            } elseif(preg_match("((?:[\-]))", $url)) {
                $params['[:' . $key . ']'] = '(?:(?P<' . $key . '>'. $keys[$key] .'))?';
                //$tes['[:' . $key . ']'] = '(?:(?P<' . $key .'>' . $value_params[$key] . '))?';
                $url = str_replace('[-]', '(?:\-)?', $url);
            }
            
            
        }
        
        //$params = str_replace(['[/:',']'], ['/', ''], $params);
        $ff = explode('/', trim($this->url,'/'));
        $ff = implode('/', $ff);
        
        
        //echo $url;
        //
        $url = str_replace(array_keys($params), $params , $url);
        //echo '<pre>' . print_r($url,1) . '</pre>';
        
        //
        if(preg_match("(^$url$)", $this->url, $results)) 
        {
            
            if(isset($results['action']))
            {
                $method = $results['action'] . 'Action';
            } else {
                $method = $route->getMethod() . 'Action';
            }
            
            if(isset($results['controller']))
            {
                $results['controller'] = ucfirst(strtolower($results['controller']));
                $cl = $route->getFile(). $results['controller'] . 'Controller';
                if(!class_exists($cl))
                {
                    return false;
                }
                
            } else {
                $cl = $route->getFile() . $route->getClass() . 'Controller';
                //echo '<pre>' . print_r($cl,1) . '</pre>';
                if(!class_exists($cl))
                {
                    return false;
                }
            }
            
            $obj = new $cl();
            
            if(!method_exists($obj, $method))
            {
                return false;
            }
            
            foreach($results as $key => $value)
            {
                if(is_numeric($key))
                {
                    unset($results[$key]);
                }
            }
            //echo '<pre>' . print_r($results,1) . '</pre>';
            $this->params = $results;
            //unset($this->params[0]);
            return true;
        }
        
        
    }
    
    public function setData($route)
    {
        $routePath = str_replace(array('[', ']',':'), array('', '',''), $route->getRoute());
        $routePath = explode('/', trim($routePath,'/'));
        
        $parseUrl = explode('/', trim($this->url,'/'));
        $this->setNameRoute($route->getNameRoute());
        
        
        //echo '<pre>' . print_r(,1) . '</pre>';
        
        $keys = array_keys($this->params);
        

        foreach($this->params as $key => $param)
        {
            $_GET[$key] = $param;
        }
        
        if(in_array('action', $keys) OR in_array('controller', $keys)){          
            $newDefault = $route->getDefaults();
            if(in_array('controller', array_keys($this->params)))
            {
                $this->class = ucfirst($this->params['controller']); 
            } else {
                $this->class = $route->getClass();
            }
            if(in_array('action',$keys) AND !empty($this->params['action']) )
            {
                $this->action = isset($this->params['action']) ? $this->params['action'] : 'index';

            } else {
                $this->action = $route->getMethod();
            }
            
            $newDefault['controller'] = $this->class;
            $newDefault['action'] = $this->action;
            $route->setDefaults($newDefault);

        } else {
                $this->class = $route->getClass();
                $this->action = $route->getMethod();
        }
        
        //echo $this->class;
        //echo $this->action;
        $this->file = $route->getFile();
        
        foreach ($route->getDefaults() as $key => $default) 
        {
            $_GET[$key] = $default;
        }
        
        $this->set('controllerClass', $this->class);
        $this->set('actionClass', $this->action);
        $this->set('file', $this->file);
        
    }
    
    public function getParams()
    {
        return $this->_params;
    }
    
    public function setCollection($collection)
    {
        if($collection != null) 
        {
            Router::$collection = $collection;
        }
    }
    
    public function getCollection()
    {
        return Router::$collection;
    }
    
    
    
}




