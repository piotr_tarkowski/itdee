<?php
namespace Router;

use MVC\Registry;

class RouterCollection
{
    private $registry;
    private $items;

    public function __construct() {
        $this->registry = Registry::getInstance();
    }
    
    public function add($name, $item) {
        $this->items[$name] = $item;
//        $tt[$name] = $item;
//        $this->registry->route = $tt;
        //echo '<pre>' . print_r($this->registry->route,1) . '</pre>';
    }
    

    public function get($name) {
        //$this->registry = Registry::getInstance();
        
        if (array_key_exists($name, $this->items)) {
            return $this->items[$name];
        } else {
            return null;
        }
    }

    public function getAll() {
        return $this->items;
    }
    
}