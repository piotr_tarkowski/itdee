<?php
namespace Router;

class Route
{
    protected $_nameRoute;
    protected $_route;
    protected $_file;
    protected $_class;
    protected $_method;
    protected $_params;
    protected $_defaults;


    public function __construct($nameRoute,$route, $defaults = array(), $params = array())
    {
        $this->setNameRoute($nameRoute);
        $this->setRoute($route);
        $this->setFile($defaults['file']);
        $this->setClass($defaults['controller']);
        $this->setMethod($defaults['action']);
        $this->setParams($params);
        $this->setDefaults($defaults);
    }
    
    public function setNameRoute($name)
    {
        $this->_nameRoute = $name;
    }
    
    public function getNameRoute()
    {
        return $this->_nameRoute;
    }
    
    public function setRoute($route)
    {
        $this->_route = $route;
    }
    
    public function getRoute()
    {
        return $this->_route;
    }
    
    public function setMethod($method) {
        $this->_method = $method;
    }

    public function getMethod() {
        return $this->_method;
    }

    public function setClass($class) {
        $this->_class = $class;
    }

    public function getClass() {
        return $this->_class;
    }

    public function setFile($file) {
        $this->_file = $file;
    }

    public function getFile() {
        return $this->_file;
    }

    public function setParams($params) {
        $this->_params = $params;
    }

    public function getParams() {
        return $this->_params;
    }

    public function setDefaults($defaults) {
        $this->_defaults = $defaults;
    }

    public function getDefaults() {
        return $this->_defaults;
    }
    
    
}