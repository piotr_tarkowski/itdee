<?php
namespace DB;

use MVC\Registry;
/**
 * Description of DB
 *
 * @author ptarkowski
 */
class Database 
{
    private $_registry;
    
    public function __construct() {
        $this->_registry = Registry::getInstance();
    }
    
    public function last_id()
    {
        return $this->connection()->lastInsertId();
    }
    
    public function connection()
    {
        $cfg = $this->getConfig();
        $dbs = new \PDO($cfg['dns'], $cfg['user'], $cfg['passwd'], array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbs->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $dbs;
    }
    
    private function getConfig()
    {
        $data = require(ROOTPATH . 'config/local.config.php');
        //$data = \Config::$database;
        return $data['pdo'];
        
    }
}
