<?php
namespace DB;




class DB 
{
    private $dns;
    private $user;
    private $passwd;
    
    private $orderBy;
    private $join;
    private $type;
    private $select;
    private $insert;
    private $from;
    private $where;
    private $update;
    private $set;
    private $values;
    private $column;
    private $delete;
    private $array = array();
    private $result;
    
    public function __construct()
    {
        $this->getConfig();
    }
    
    public function clear()
    {
        unset($this->orderBy);
        unset($this->join);
        unset($this->type);
        unset($this->select);
        unset($this->insert);
        unset($this->from);
        unset($this->where);
        unset($this->update);
        unset($this->set);
        unset($this->values);
        unset($this->column);
        unset($this->delete);
        unset($this->array);
        unset($this->result);
    }
    
    public function __toString()
    {
        $_join = '';
        $query = '';
        
        if(!empty($this->join))
        {
            foreach($this->join as $join)
            {
                $_join .= $join;
            }
        }
        
        switch ($this->type)
        {
            case 'select':
                $query .= 'SELECT ';
                foreach($this->select as $key => $_select)
                {
                    if ($key == 0) {
                        $query .= $_select;
                    } else {
                        $query .= "," . $_select;
                    }
                }
                $query .= " FROM " . (string) $this->from . $_join;
                break;
            case 'insert':
                $query .= 'INSERT INTO ' . $this->insert;
                if (!empty($this->set)) {
                    foreach ($this->set as $k => $v) 
                    {
                        $column[] = $v;
                        $values[] = ':'.$v;
                    }
                    $this->column = implode(",", $column);
                    $this->values = array(implode(",", $values));
                }
                $query .= ' (' . $this->column . ') VALUES ';
                $comma = '';
                foreach ($this->values as $element) {
                    $query .= $comma . '(' . $element . ')';
                    $comma = ',';
                }
                break;
            case 'delete':
                $query .= 'DELETE ' . $this->delete;
                $query .= ' FROM ' . (string)$this->from  . $_join;
 
                break;
            case 'update':
                $query .= 'UPDATE ' . (string) $this->update . " " . $_join . " SET ";
                $comma = '';
                foreach ($this->set as $v => $element) {
                    $query .= $comma . $v . " = " . $element;
                    $comma = ',';
                }

                break;
        }
        
        if (!empty($this->where)) {
            $query .= " WHERE ";
            foreach ($this->where as $key => $val) {
                $query .= $val . " ";
            }
        }
        if (!empty($this->group)) {
            $query .= " GROUP BY " . $this->group;
        }
        if (!empty($this->orderBy)) {
            $query .= " ORDER BY ";
            $comma = '';
            foreach ($this->orderBy as $element) {
                $query .= $comma . $element;
                $comma = ',';
            }
        }
        
        return $query;
        
    }
    
    public function query($query)
    {
        
        $db = $this->connection();
        $tablica = '';
        $this->result = $db->prepare($query);
        if(empty($this->array))
        {
            $this->array = array();
        }
        //echo '<pre>' . print_r($this->result,1) . '</pre>';
        $this->result->execute($this->array);
        
        return $this;
    }
    
    public function fetch()
    {
        
        return $this->result->fetch(\PDO::FETCH_ASSOC);
    }
    
    public function fetchAll()
    {
        
        return $this->result->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function fetchObj()
    {
        
        return $this->result->fetchObject(\PDO::FETCH_ASSOC);
    }
    
    public function connection()
    {
        
        $dbs = new \PDO($this->dns, $this->user, $this->passwd);
        return $dbs;
        
    }
    
    private function getConfig()
    {
        $data = \Config::$database;
        $this->dns = $data['dns'];
        $this->user = $data['user'];
        $this->passwd = $data['passwd'];
    }
    
    
    public function select($select = '*')
    {
        $this->clear();
        $this->type = 'select';
        $this->select[] = $select;
        return $this;
    }
    
    public function from($from)
    {
        $this->from = $from;
        return $this;
    }
    
    public function insert($tableName = null)
    {
        $this->clear();
        $this->type = 'insert';
        $this->insert = $tableName;
        return $this;
    }
    
    public function delete($columnName = NULL) {
        $this->clear();
        $this->type = 'delete';
        $this->delete = $columnName;
        return $this;
    }
    
    public function orderBy($columnName, $dir = 'ASC') 
    {

        $this->orderBy[] = $columnName . " " . $dir;
        return $this;
    }

    public function where($columnName, $comparison = FALSE, $value = FALSE, $connectingConditions = 'and', $lowerBrackets = false)
    {
        $columnValue = ':' . $columnName;
        if (($comparison === FALSE) and ( $value === FALSE)) {
            $comparison = "";
            $value = "";
            $lowerBrackets = true;
        }

        if (empty($this->where)) {
            $connectingConditions = "";
        }
        
        if (!($value === null or ( $value == "null" or $value == "NULL"))) {
            if (!$lowerBrackets) {
                $value = "" . $value . "";
            }
        }
        
        $this->array[$columnValue] = $value;
        if($value == false)
        {
            $columnValue = '';
        }
        
        
        $this->where[] = $connectingConditions . " " . $columnName . " " . $comparison . " " . $columnValue . " ";

        return $this;
    }
    
    public function join($type, $tableName, $connectingField) 
    {

        $this->join[] = " " . $type . " JOIN " . $tableName . " ON " . $connectingField;
        return $this;
    }
    
    public function update($dane) 
    {
        $this->type = 'update';
        $this->update = $dane;
        return $this;
    }
    
    public function column($dane = NULL) 
    {

        $this->column = $dane;
        return $this;
    }

    public function values($dane) 
    {

        $this->values[] = $dane;
        return $this;
    }
    
    public function set($pole, $value, $nawias = TRUE) 
    {

        
        if ((empty($value) or ( $value == 'null' or $value == 'NULL')) and ! is_numeric($value)) 
        {
            $nawias = False;
            $value = 'NULL';
        }
        $columnValue = ':'.$pole;
        $this->array[$pole] = $value;
        $this->set[$pole] = $nawias ? "" . $pole . "" : $pole;
        return $this;
    }
    
}
