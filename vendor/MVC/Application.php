<?php
namespace MVC;

use MVC\Registry;
use DB\Database;


class Application 
{
    private $_registry;
    private $prefix;
    
    public function __construct() {
        $this->_registry = Registry::getInstance();
    }
    
    public function setConfig($name, $data)
    {
        if(!empty($data))
        {
            $this->set($name, $data);
            
        }
    }
    
    protected function addNamespace($modules)
    {
        foreach($modules as $module)
        {
            
            $this->prefix[$module] = 'module\\' . $module;
        }
        
    }
    
    public function setModule()
    {
        $config = $this->get('config');
        $this->addNamespace($config['modules']);
        $this->set('modules', $this->prefix);
        //echo '<pre>' . print_r($this->get('modules'),1) . '</pre>';
    }
    
    private function loadClass($class)
    {
		
        $prefix = $class;
        while(false !== $pos = strpos($prefix, '\\'))
        {
            
            $prefix = substr($class, 0, $pos);
            //$this->set('moduless',$this->prefix);
            //echo $prefix;
            // the rest is the relative class name
            $_GET['module'] = $prefix;
            $relative_class = substr($class, $pos + 1);
            //echo $relative_class;
            $mapped_file = $this->loadMappedFile($prefix, $relative_class);
            if ($mapped_file) {
                return $mapped_file;
            }
            
            $prefix = rtrim($prefix, '\\');
        }
        
        return false;
        
        
    }
    
    private function loadMappedFile($prefix, $relative_class)
    {
        //
        
        if (isset($this->prefix[$prefix]) === false) {
            return false;
        }
        
        //foreach ($this->prefix[$prefix] as $base_dir) 
        //{
            
            $file = ROOTPATH . $this->prefix[$prefix] . '/src'
                  . '/'
                  . str_replace('\\', '/', $relative_class)
                  . '.php';
            if(($relative_class) == 'Error\Error')
            {
                $file = str_replace('src/', '', $file);
            }
			//echo ROOTPATH;
			$file = str_replace('\\', '/', $file);
            //echo $file;
			
            if ($this->requireFile($file)) 
            {
                
                return $file;
            }
        //}


        return false;
    }
    
    private function requireFile($file)
    {
        if(file_exists($file))
        {
            require $file;
            return true;
        }
        return false;
    }
    
    private function getSetting()
    {
        $db = new Database();
        $dbh = $db->connection();
        $stmt = $dbh->prepare('select * from setting');
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach($result as $k => $v)
        {
            $tes[$v['option_name']] = $v['option_value'];
        }
        
        return $tes;
    }
    
    public function run()
    {
        $this->setModule();
        $sett = $this->getSetting();
        $this->set('setting', $sett);

        $protocol = $_SERVER['REQUEST_SCHEME'];        
        $url = $_SERVER['SERVER_NAME'];
        $urlPath = $protocol . '://' . $url;
        //DEFINE('URL', $sett['siteurl']);
        DEFINE('URL', $urlPath);

        define('ROUTE_PAGE', $sett['route_page']);
        spl_autoload_register([$this, 'loadClass']);
    }
    
    private function set($name, $value)
    {
        $this->_registry->$name = $value;
    }
    
    private function get($name)
    {
        if(!empty($name))
        {
            return $this->_registry->$name;
        }
    }
}
