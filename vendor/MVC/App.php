<?php
namespace MVC;

use Router\Router;
use Router\Route;
use Router\RouterCollection;
use MVC\Registry;
//use Application\Error\Error;
use Auth\Session;
use View\View;
use DB\Database;

class App
{
    public $_registry;
    private $url;
    
    public function __construct() 
    {
        
        $this->routing();
        
    }
    
    private function routing()
    {
        /*
         * Router
         */
        $cfg = require(ROOTPATH . 'config/module.config.php');
        
        $collection = new RouterCollection();
        foreach ($cfg['router'] as $key => $value)
        {
            $collection->add($key, new Route(
                $key,
                $value['route'],
                $value['options'],
                $value['params']
            ));

        }
        $this->set('route', $cfg['router']);


        $this->url = filter_input(INPUT_SERVER, 'REQUEST_URI');
        //echo '<pre>' . print_r($_SERVER,1) . '</pre>';
        try
        {
            /*
             * class Router <- szuka najlepszego routera
             * pliki config -> module.config.php
             */
            $router = new Router($this->url, $collection);
            $router->run();
            /*
             * checkRole sprawdza czy użytkownik ma dostęp do danego modułu
             * pliki konfiguracyjne: acl.config.php i module.config.php
             * 
             */
            if($this->checkRole($router->getNameRoute()))
            {
                $this->init();
            }
            //$regs = Registry::getInstance();
            //echo '<pre>' . print_r($regs,1) . '</pre>';
            $this->views();
            //echo '<pre>' . print_r($regs,1) . '</pre>';
            
        } catch (\Exception $e) {
            /*
             * obsługa błedów
             */
            //$controller = new Error();
            
            $_GET['controller'] = 'Error';
            $_GET['action'] = 'index';
            $_GET['file'] = 'Application\Controller\\';
            $this->set('controllerClass', 'Error');
            $this->set('actionClass', 'index');
            $this->set('file', 'Application\Controller\\');
            
            
            $error = array(
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
                'line'    => $e->getLine(),
                'file'    => $e->getFile(),
                'trace'   => $e->getTraceAsString(),
            );

            $_GET['param'] = $error;
            //$this->set(error, $error);
            //$errorController = $_GET['file'] . $_GET['controller'];
            //$app = new $errorController();
            //if(method_exists($app, $_GET['action'].'Action' ))
            //{
            //    call_user_func([$app, $_GET['action'] ], $error);
            //}


            $this->init();
            $this->views();
            exit();
            
        }
        
        //echo '<pre>' . print_r($_GET,1) . '</pre>';
        
    }
    
    private function getConfig($name)
    {
        if(isset($name))
        {
            $cfg = require(ROOTPATH . 'config/module.config.php');
            $test = $this->get('config');
            $cfgg = array_merge($test,$cfg['settings']);
            //echo '<pre>' . print_r($test,1) . '</pre>';
            
            //$this->set('config', $cfgg);
            return $cfg['settings'][$name];
        }
        
    }
    /*
     * init <- inicjuje controller oraz akcję
     */
    private function init()
    {
        
        $controller = $this->getController();
        $action = $this->getAction();
        
//        echo $controller;
//        echo $action;

        if(class_exists($controller))
        {
            $app = new $controller();
            if(method_exists($app, $action))
            {
                //$app->$action();
                
                
                call_user_func([$app,$action]);
                return true;
            }
        }
        
        
        throw new \Exception('Dany Kontroler lub Metoda nie istnieje.', 404);
    }
    
    private function checkRole($rolename)
    {
        //echo '<pre>' . print_r($_SERVER,1) . '</pre>';
        $acl = require(ROOTPATH . 'config/acl.config.php');
        $sess = new Session();
        $sess->clear();
        $result = $sess->read();
        //echo '<pre>' . print_r(filter_input_array(INPUT_SESSION), 1) . '</pre>';

        if(empty(filter_input(INPUT_COOKIE, 'sessionid')))
        {
            $sess->destroy();
            unset($_SESSION);
            $result = $sess->read();
        }
        //echo '<br><br><br>' . filter_input(INPUT_SESSION, 'sessionid');
        $role = 'guest';
        $msg = 'Dostęp zabroniony! <br />Nie posiadasz wystarczających uprawnień do ogladania tej strony.';

        if(filter_input(INPUT_COOKIE, 'sessionid') && filter_input(INPUT_COOKIE, 'sessionid') == $sess->sessionid )
        {
            if(!empty($result['session_id']) && $result['session_id'] == $_SESSION['sessionid'] )
            {
                
                $role = $result['role'];
                $acl['user'] = array_merge($acl['user'], $acl['guest']);
                if(isset($_SESSION['role']) && $_SESSION['role'] == 'admin')
                {
                    $acl['admin'] = array_merge($acl['admin'], $acl['user']);
                }
            } else {
                
                //throw new \Exception($msg, 400);
            }
        }
        

        //echo '<pre>' . print_r($result, 1) . '</pre>';
        
        if(in_array($rolename, $acl[$role]))
        {
            return true;
        }
        throw new \Exception($msg, 403);
    }
    
    /*
     * obsługa dostępów dla roli "User"
     */
    private function access($name)
    {
        
        
        if(isset($_SESSION['sessionid']) && $_SESSION['role'] == 'user')
        {
            //$access = $_SESSION['access'];
            $dbh = new Database();
            $db = $dbh->connection();
            
            $stmt = $db->prepare('select * from up_grupy');
            $stmt->execute();
            $grRes = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            
            //$usr = $_SESSION['access'];
            foreach($grRes as $k => $v)
            {
                if(!empty($_SESSION['access'][$v['id']]))
                {
                    $dd[$v['controller']] = $_SESSION['access'][$v['id']];
                } else {
                    $dd[$v['controller']] = 0;
                }
                
            }
            //echo '<pre>' . print_r($dd,1) . '</pre>';
            if(isset($dd[$name])){
                return $dd[$name];
            } else {
                
                return true;
            }

        }
        
    }
    
    /*
     * obsługa wyświetlania layoutu oraz odpowiedniego contentu
     */
    private function views()
    {
        //echo '<br><br>' ;
//        $fff = get_browser();
//        echo '<pre>' . print_r($fff,1) . '</pre>';
        
        $class = isset($_GET['controller']) ? $_GET['controller'] : '';
        $method = isset($_GET['action']) ? $_GET['action'] : '';
        $file = strtolower($class . '/' . $method . '.phtml');
        
        $moduleName = substr($_GET['file'],0, strpos($_GET['file'], '\\'));
        
        $view_path = $this->getConfig('view_path');
        $modules = $this->get('modules');
        //echo '<br><pre>' . print_r($modules, 1) . '</pre>';
        //echo $moduleName;
        if(in_array($moduleName, array_keys($modules)))
        {
            $module = $modules[$moduleName];
            
            if(file_exists(str_replace('\\', '/', ROOTPATH . $module . $view_path . strtolower($moduleName) . DIRECTORY_SEPARATOR . $file)))
            {
                $file = ROOTPATH . $module . $view_path . strtolower($moduleName) . DIRECTORY_SEPARATOR . $file;

            } else {
                $file = ROOTPATH . $module . $view_path . $file;

            }
        }
        $file = str_replace('\\', '/', $file);
        //echo $file;
        
        $layout = $this->getConfig('layout');
        //echo '<br><br><br>';
       // echo $layout;

        if(file_exists($file))
        {
            //var_dump($this->get('variables'));
            //echo $file;
            $view = new View();
            $view->render($layout,$this->get('variables'), $file);
            return true;
        }

        throw new \Exception('Brak zawartości do wyświetlenia', 404);

    }
    /*
     * 
     * getController <- pobiera odpowiedni Kontroler z Router
     */
    private function getController()
    {
        return $this->get('file') . $this->get('controllerClass') . 'Controller';
    }
    /*
     * getAction <- pobiera odpowiednią metodę z Router
     */
    private function getAction()
    {
        return $this->get('actionClass') . 'Action';
    }
    
    public function get($name)
    {
        $new = Registry::getInstance();
        //echo '<br><pre>' . print_r($new, 1) . '</pre>';
        return $new->$name;
    }
    
    public function set($name, $value)
    {
        $new = Registry::getInstance();
        $new->$name = $value;
    }
    
}

