<?php
namespace MVC\Model;

/**
 * Description of LoadModel
 *
 * @author ptarkowski
 */


class LoadModel 
{
    
    public function load($name) 
    {
        $path = str_replace('Controller', 'Model', $_GET['file']) . ucfirst($name);
        //$obj = new $path();
        //
        if(class_exists($path))
        {
            
        
            $obj = new $path();
            //echo '<pre>' . print_r($obj->getAllUsersData(), 1) . '</pre>';
            return $obj;
        }
        
        throw new \Exception('Dany Model nie może być załadowany.', 404);
    }
    
    
}
