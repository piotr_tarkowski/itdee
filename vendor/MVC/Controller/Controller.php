<?php
namespace MVC\Controller;

use MVC\Registry;
use MVC\Model\LoadModel;
use DB\Database;
use Auth\Session;
use View\ViewModel;

class Controller
{
    const test = 'test';

    protected $user = array();
    public $view = null;
    //public $_registry;
    
    public function __construct()
    {

        $this->view = new \stdClass();
        //$this->_registry = Registry::getInstance();
        //$this->view = new ViewModel();

    }

    protected function session()
    {
        $sess = new Session();
        return $sess;
    }

    /**
     * @param $name Nazwa modelu
     * @return LoadModel
     */
    public function model($name)
    {
        $model = new LoadModel();
        return $model->load($name);
    }
    
    public function userData($name)
    {
        $sess = new Session();
        if($sess->$name)
        {
            
            return $sess->$name;
        }
    }
    
    public function setUsrData($name, $value)
    {
        
        if(isset($value))
        {
            $sess = new Session();
            $sess->$name = $value;
        }
    }

    public function setTemplate($name)
    {
        $view = ViewModel::getInstance();
        $view->setTemplate($name);
    }
    
    public function param($param)
    {
        if(isset($_GET[$param]))
        {
            /* @var $param type */
            return $_GET[$param];
        }
    }

    public function getAllParams()
    {
        return $_GET['param'];
    }
    
    public function set($name, $value)
    {
        $registry = Registry::getInstance();
        $registry->$name = $value;
    }
    
    public function get($name) 
    {
        
        if(!empty($name))
        {
            $registry = Registry::getInstance();
            return $registry->$name;
        }
        return false;
    }


    protected function getNewDBConnect()
    {
        $db = new Database();
        return $db;
    }

    public function __destruct()
    {
        //var_dump($this->view);
        $registry = Registry::getInstance();
        $registry->variables = $this->view;
    }

}
