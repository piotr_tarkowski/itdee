<?php
return array(
    'guest' => array(
        'home',
        'login',
        'pages',
        'lost-password',
        'register',
        'activated',
        'blog',
		'contact',
    ),
    'user' => array(
        //'application',
        'profil',
        'editPr',
        'logout',
    ),
    'admin' => array(
        'admin',
    ),
);

