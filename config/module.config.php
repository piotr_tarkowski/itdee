<?php
return array(
    'router' => [
        'home' => [
            'route' => '/',
            'options' => [
                'file'       => 'Application\Controller\\',
                'controller' => 'Home',
                'action'     => 'index',
            ],
            'params' => [
                
            ],
        ],
        'login' => [
            'route' => '/login',
            'options' => [
                'file'       => 'Admin\Controller\\',
                'controller' => 'Admin',
                'action'     => 'login'
                
            ],
            'params' => [
                
            ],
        ],
        'logout' => [
            'route' => '/logout',
            'options' => [
                'file'       => 'Application\Controller\\',
                'controller' => 'Home',
                'action'     => 'logout',
            ],
            'params' => [
                
            ],
        ],
        'register' => [
            'route' => '/register',
            'options' => [
                'file'       => 'Admin\Controller\\',
                'controller' => 'Admin',
                'action'     => 'register'
                
            ],
            'params' => [
                
            ],
        ],
        
        'activated' => [
            'route' => '/activated[/:id]',
            'options' => [
                'file'       => 'Admin\Controller\\',
                'controller' => 'Admin',
                'action'     => 'activated'
                
            ],
            'params' => [
                'id' => '\S+',
            ],
        ],
        'lost-password' => [
            'route' => '/lost-password',
            'options' => [
                'file'       => 'Admin\Controller\\',
                'controller' => 'Admin',
                'action'     => 'lost'
                
            ],
            'params' => [
                
            ],
        ],
        'admin' => [
            'route' => '/admin[/:controller][/:action][/:param]',
            'options' => [
                'file'       => 'Admin\Controller\\',
                'controller' => 'Admin',
                'action'     => 'index'
            ],
            'params' => [
                'controller'  => '\w+',
                'action'    => '\w+',
                'param'        => '\d+'
            ],
        ],
		'contact' => array(
            'route' => '/contact',
            'options' => array(
                'file'       => 'Application\Controller\\',
                'controller' => 'Contact',
                'action'     => 'veryfi'
            ),
            'params' => array(
                
            ),
        ),
        'profil' => array(
            'route' => '/profil',
            'options' => array(
                'file'       => 'Application\Controller\\',
                'controller' => 'Profil',
                'action'     => 'index'
            ),
            'params' => array(
                
            ),
        ),
        'editPr' => array(
            'route' => '/profil/edycja',
            'options' => array(
                'file'       => 'Application\Controller\\',
                'controller' => 'Profil',
                'action'     => 'edycja'
            ),
            'params' => array(
                
            ),
        ),
        'blog' => array(
            'route' => '/blog[/:action][/:param]',
            'options' => array(
                'file'       => 'Application\Controller\\',
                'controller' => 'Blog',
                'action'     => 'wpisy'
            ),
            'params' => array(
                'action'    => '\w+',
                'param'        => '\d+',
            ),
        ),
        
//        'application' => [
//            'route' => '[/:controller][/:action][/:param]',
//            'options' => [
//                'file'       => 'Application\Controller\\',
//                'controller' => 'Home',
//                'action'     => 'index'
//                
//            ],
//            'params' => [
//                'controller'  => '\w+',
//                'action'      => '\w+',
//                'param'          => '\d+'
//            ],
//        ],

        'pages' => [
            'route' => ROUTE_PAGE,
            'options' => [
                'file'       => 'Application\Controller\\',
                'controller' => 'Pages',
                'action'     => 'index'
            ],
            'params' => [
                'slug'      => '\w+',
                'id'        => '\d+',
            ],
        ],
        
        
        
    ],
    'controllers' => [
        
    ],
    'settings' => [
        'layout' => '/view/template/',
        'error' => '/view/error/index.phtml',
        'view_path' => '/view/',
        'view_manager' => [
            
            
        ],
    ],
    

);
