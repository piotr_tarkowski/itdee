-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 29 Lis 2020, 18:12
-- Wersja serwera: 5.7.32-0ubuntu0.16.04.1
-- Wersja PHP: 7.0.33-0ubuntu0.16.04.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `admin_itdee`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block`
--

CREATE TABLE `block` (
  `id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8_polish_ci NOT NULL,
  `position` varchar(80) COLLATE utf8_polish_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block`
--

INSERT INTO `block` (`id`, `name`, `position`, `type`, `menu_id`, `content_id`) VALUES
(1, 'Menu główne', 'main', 'menu', 1, NULL),
(2, 'Footer One', 'footer-one', 'menu', 5, NULL),
(3, 'Footer second', 'footer-second', 'clear', NULL, 4),
(4, 'Footer third', 'footer-third', 'clear', NULL, 2),
(5, 'Footer four', 'footer-four', 'clear', NULL, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block-content`
--

CREATE TABLE `block-content` (
  `id` int(11) NOT NULL,
  `title` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  `text` text COLLATE utf8_polish_ci NOT NULL,
  `showTitle` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block-content`
--

INSERT INTO `block-content` (`id`, `title`, `text`, `showTitle`) VALUES
(1, 'Test 1', 'Standardowy blok Lorem Ipsum, używany od XV wieku, jest odtworzony niżej dla zainteresowanych. Fragmenty 1.10.32 i 1.10.33 z „de Finibus Bonorum et Malorum” \r\n                            Cycerona, są odtworzone w dokładnej, oryginalnej formie, wraz z angielskimi tłumaczeniami H. Rackhama z 1914 roku.', 0),
(2, 'Test 2', 'Standardowy blok Lorem Ipsum, używany od XV wieku, jest odtworzony niżej dla zainteresowanych. Fragmenty 1.10.32 i 1.10.33 z „de Finibus Bonorum et Malorum” \n                            Cycerona, są odtworzone w dokładnej, oryginalnej formie, wraz z angielskimi tłumaczeniami H. Rackhama z 1914 roku.', 0),
(4, 'Kontakt', 'Kontakt z nami: <b>kontakt@itdee.pl</b>', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `menu`
--

INSERT INTO `menu` (`id`, `name`) VALUES
(1, 'Główne'),
(5, 'Footer');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu-link`
--

CREATE TABLE `menu-link` (
  `id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8_polish_ci NOT NULL,
  `parent` int(11) DEFAULT '0',
  `ordering` int(11) NOT NULL,
  `post_slug` varchar(80) COLLATE utf8_polish_ci DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `url` varchar(150) COLLATE utf8_polish_ci DEFAULT NULL,
  `target` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `menu-link`
--

INSERT INTO `menu-link` (`id`, `name`, `parent`, `ordering`, `post_slug`, `post_id`, `url`, `target`, `type`, `menu_id`) VALUES
(4, 'O nas', 0, 3, 'about', 2, NULL, NULL, 'page', 1),
(5, 'Portfolio', 0, 1, 'portfolio', 1, NULL, NULL, 'page', 1),
(13, 'Portfolio', 0, 2, 'portfolio', 1, NULL, NULL, 'page', 5),
(14, 'O nas', 0, 4, 'about', 2, NULL, NULL, 'page', 5),
(15, 'Strona Główna', 0, 1, NULL, NULL, '/', '', 'custom_url', 5),
(22, 'Kontakt', 0, 4, 'kontakt', 3, NULL, NULL, 'page', 1),
(23, 'Kontakt', 0, 5, 'kontakt', 3, NULL, NULL, 'page', 5),
(24, 'Oferta', 0, 2, 'oferta', 7, NULL, NULL, 'page', 1),
(25, 'Oferta', 0, 3, 'oferta', 7, NULL, NULL, 'page', 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  `data` text COLLATE utf8_polish_ci NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `module`
--

INSERT INTO `module` (`id`, `name`, `data`, `active`) VALUES
(2, 'portfolio', 'a:4:{i:0;a:3:{s:5:"title";s:18:"Wykonanie projektu";s:3:"img";s:23:"blobid1488538978314.png";s:3:"url";s:15:"http://itdee.pl";}i:1;a:3:{s:5:"title";s:15:"Wykonanie bloga";s:3:"img";s:9:"blogs.jpg";s:3:"url";s:15:"http://itdee.pl";}i:2;a:3:{s:5:"title";s:39:"Realizacja i wdrożenie strony ITDEE.pl";s:3:"img";s:23:"blobid1488537166123.png";s:3:"url";s:15:"http://itdee.pl";}i:3;a:3:{s:5:"title";s:23:"Wykonanie projektu Dark";s:3:"img";s:12:"mceclip2.png";s:3:"url";s:15:"http://itdee.pl";}}', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `panel`
--

CREATE TABLE `panel` (
  `id` int(11) NOT NULL,
  `title` varchar(120) COLLATE utf8_polish_ci NOT NULL,
  `text` text COLLATE utf8_polish_ci NOT NULL,
  `image` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL,
  `imgAlt` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL,
  `imgTitle` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL,
  `orders` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `panel`
--

INSERT INTO `panel` (`id`, `title`, `text`, `image`, `imgAlt`, `imgTitle`, `orders`, `active`) VALUES
(1, 'Wizytówki', 'Wizytówki internetowe, to proste strony internetowe, które zazwyczaj posiadają od 3 do 4 podstron z najważniejszymi informacjami, takimi jak: kontakt, kilka swój o firmie, czy też  z aktualną ofertą firmy.', 'panel59312a0aa85fa.png', 'Profesjonalne wizytówki internetowe', 'Proste wizytówki internetowe dla każdego', 1, 1),
(2, 'Strony internetowe', 'Profesjonalne tworzenie stron internetowych, blogów, rozbudowane portale i wszelkiego rodzaju strony internetowe. Wszystko na autorskim systemie cms, lub jednym z popularnych (wordpress, drupal, joomla). Szablony stron internetowych są w pełni responsywne czyli przystosowane do wersji mobilnych np. smartfony.', 'slider1.png', 'Tasie strony internetowe', 'Autorskie strony internetowe', 2, 1),
(3, 'Prace programistyczne', 'Zajmujemy się również wszelakimi pracami programistycznymi, zaczynając od modyfikacji/odświeżaniem stron internetowych, rozbudowa istniejących, lub wykonanie nowych wtyczek, cięcie szablonów stron, oraz wiele innych usług. Bez problemu wykonamy przeniesienie ( migracja ) twojej strony internetowej na nowy system cms np joompa -> wordpress.', 'panel5931196bc46ce.png', 'Wszelkie prace programistyczne', 'Wszelkie prace programistyczne', 3, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  `name` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `content` text COLLATE utf8_polish_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `meta_title` varchar(80) COLLATE utf8_polish_ci DEFAULT NULL,
  `meta_keywords` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_polish_ci,
  `author` int(11) NOT NULL,
  `modul` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `posts`
--

INSERT INTO `posts` (`id`, `title`, `name`, `date_create`, `date_modified`, `content`, `type`, `status`, `meta_title`, `meta_keywords`, `meta_description`, `author`, `modul`) VALUES
(1, 'Portfolio', 'portfolio', '2017-01-31 15:40:35', '2017-06-14 14:19:07', '', 'page', 'publish', 'Portfolio', 'portfolio, prace, szkice, przykłady stron internetowych, itdee', 'Portfolio pokazuje nasze zrobione prace, przykłady, lub szkice szablonów stron internetowych.', 1, 'portfolio'),
(2, 'O nas', 'about', '2017-02-01 10:42:46', '2017-03-28 12:45:00', '<p>Specjalizujemy się w wykonywaniu stron internetowych. Tworzymy strony internetowe o wszelakiej tematyce, zaczynając od prostych wizyt&oacute;wek internetowych, aż po rozbudowane aplikacje internetowe.<br />Do projektowania stron wykorzystujemy nowoczesne technologie oraz obecne trendy.&nbsp;<br />Spis projekt&oacute;w i prac znajdziesz w&nbsp;<a title="Nasze prace." href="/portfolio">Portfolio</a>, zaś ofertę przejrzysz w&nbsp;<a title="Nasza oferta" href="/oferta">Oferta</a>.</p>\r\n<p>Nasz zesp&oacute;ł to młodzi ludzie dla kt&oacute;rych programowanie to nie tylko praca, ale r&oacute;wnież hobby. Do każdego projektu podchodzimy z ogromnym entuzjazmem, kt&oacute;ry połączony z wiedzą, oraz wieloletnim doświadczeniem pozwala podejść do każdego nowego projektu z energią i świeżymi pomysłami.</p>\r\n<p>Jeśli masz pytania dane kontaktowe znajdziesz w dziale&nbsp;<a title="Kontakt z nami." href="/kontakt">Kontakt</a></p>', 'page', 'publish', 'O Itdee.pl', 'about, o nas, o firmie, itdee', 'Kilka słów o nas, oraz czym się zajmujemy.', 1, NULL),
(3, 'Kontakt', 'kontakt', '2017-02-01 11:53:10', '2017-04-26 15:22:53', '<p>Dane dzięki kt&oacute;rym w łatwy spos&oacute;b skontaktujesz się z nami:</p>\r\n<ul style="list-style-type: circle;">\r\n<li>Email:&nbsp;<a href="mailto: kontakt@itdee.pl">kontakt@itdee.pl</a></li>\r\n<li>Facebook:&nbsp;<a href="https://www.facebook.com/strony.itdee" target="_blank" rel="noopener noreferrer">https://www.facebook.com/strony.itdee</a></li>\r\n</ul>\r\n<p>Możesz także wysłać nam wiadomość poprzez poniższy formularz.</p>\r\n<p>Naszą ofertę znajdziesz na podstronie&nbsp;<a title="Oferta" href="/oferta">Oferta</a>, jeśli chcesz jeszcze raz zobaczyć nasze prace, znajdziesz je w&nbsp;<a title="Strony internetowe, wizyt&oacute;wki." href="/portfolio">Portfolio</a>, zaś kilka sł&oacute;w kim jesteśmy czym się zajmujemy lub jak pracujemy możesz znaleźć w kategorii&nbsp;<a title="Kilka sł&oacute;w o nas, czyli kim jesteśmy" href="/about">O nas</a>.</p>', 'page', 'publish', 'Kontakt', 'kontakt, formularz, contact, itdee', 'Dane kontaktowe do nas, oraz formularz kontaktowy.', 1, 'contact'),
(5, 'Testowy post', 'testowy_post', '2017-02-16 15:02:22', '2017-03-16 10:07:27', '<p style="text-align: center;"><iframe src="//www.youtube.com/embed/-DOrtlfQPpM" width="560" height="314" allowfullscreen="allowfullscreen"></iframe><br /><span style="font-size: 8pt;"><em>Najnowszy odcinek Ucha Prezesa</em></span></p>\r\n<p>Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem pr&oacute;bnej książki. Pięć wiek&oacute;w p&oacute;źniej zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w. wraz z publikacją arkuszy Letrasetu, zawierających fragmenty Lorem Ipsum, a ostatnio z zawierającym r&oacute;żne wersje Lorem Ipsum oprogramowaniem przeznaczonym do realizacji druk&oacute;w na komputerach osobistych, jak Aldus PageMaker.</p>\r\n<p style="text-align: center;"><a href="http://localhost/dobra/public/img/gal/blobid1488533946885.png" target="_blank" rel="noopener noreferrer"><img title="graf2.png" src="http://localhost/dobra/public/img/gal/blobid1488533946885.png" alt="" width="650" height="357" /></a>&nbsp;</p>\r\n<p>W przeciwieństwie do rozpowszechnionych opinii, Lorem Ipsum nie jest tylko przypadkowym tekstem. Ma ono korzenie w klasycznej łacińskiej literaturze z 45 roku przed Chrystusem, czyli ponad 2000 lat temu! Richard McClintock, wykładowca łaciny na uniwersytecie Hampden-Sydney w Virginii, przyjrzał się uważniej jednemu z najbardziej niejasnych sł&oacute;w w Lorem Ipsum &ndash; consectetur &ndash; i po wielu poszukiwaniach odnalazł niezaprzeczalne źr&oacute;dło: Lorem Ipsum pochodzi z fragment&oacute;w (1.10.32 i 1.10.33) &bdquo;de Finibus Bonorum et Malorum&rdquo;, czyli &bdquo;O granicy dobra i zła&rdquo;, napisanej właśnie w 45 p.n.e. przez Cycerona. Jest to bardzo popularna w czasach renesansu rozprawa na temat etyki. Pierwszy wiersz Lorem Ipsum, &bdquo;Lorem ipsum dolor sit amet...&rdquo; pochodzi właśnie z sekcji 1.10.32.</p>\r\n<p style="text-align: center;"><img title="graf1.png" src="http://localhost/dobra/public/img/gal/blobid1488533934394.png" alt="" width="650" height="333" />&nbsp;</p>\r\n<p>Standardowy blok Lorem Ipsum, używany od XV wieku, jest odtworzony niżej dla zainteresowanych. Fragmenty 1.10.32 i 1.10.33 z &bdquo;de Finibus Bonorum et Malorum&rdquo; Cycerona, są odtworzone w dokładnej, oryginalnej formie, wraz z angielskimi tłumaczeniami H. Rackhama z 1914 roku.</p>\r\n<p style="text-align: center;"><img title="graf2.png" src="http://localhost/dobra/public/img/gal/blobid1488534511194.png" width="866" height="476" /></p>\r\n<p>Og&oacute;lnie znana teza głosi, iż użytkownika może rozpraszać zrozumiała zawartość strony, kiedy ten chce zobaczyć sam jej wygląd. Jedną z mocnych stron używania Lorem Ipsum jest to, że ma wiele r&oacute;żnych &bdquo;kombinacji&rdquo; zdań, sł&oacute;w i akapit&oacute;w, w przeciwieństwie do zwykłego: &bdquo;tekst, tekst, tekst&rdquo;, sprawiającego, że wygląda to &bdquo;zbyt czytelnie&rdquo; po polsku. Wielu webmaster&oacute;w i designer&oacute;w używa Lorem Ipsum jako domyślnego modelu tekstu i wpisanie w internetowej wyszukiwarce &lsquo;lorem ipsum&rsquo; spowoduje znalezienie bardzo wielu stron, kt&oacute;re wciąż są w budowie. Wiele wersji tekstu ewoluowało i zmieniało się przez lata, czasem przez przypadek, czasem specjalnie (humorystyczne wstawki itd).</p>', 'post', 'publish', '', '', '', 1, NULL),
(6, 'Testowy post', 'testowy_post', '2017-02-16 15:02:01', '2017-03-14 10:34:25', '<p>Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem pr&oacute;bnej książki. Pięć wiek&oacute;w p&oacute;źniej zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w. wraz z publikacją arkuszy Letrasetu, zawierających fragmenty Lorem Ipsum, a ostatnio z zawierającym r&oacute;żne wersje Lorem Ipsum oprogramowaniem przeznaczonym do realizacji druk&oacute;w na komputerach osobistych, jak Aldus PageMaker.</p>\r\n<p>W przeciwieństwie do rozpowszechnionych opinii, Lorem Ipsum nie jest tylko przypadkowym tekstem. Ma ono korzenie w klasycznej łacińskiej literaturze z 45 roku przed Chrystusem, czyli ponad 2000 lat temu! Richard McClintock, wykładowca łaciny na uniwersytecie Hampden-Sydney w Virginii, przyjrzał się uważniej jednemu z najbardziej niejasnych sł&oacute;w w Lorem Ipsum &ndash; consectetur &ndash; i po wielu poszukiwaniach odnalazł niezaprzeczalne źr&oacute;dło: Lorem Ipsum pochodzi z fragment&oacute;w (1.10.32 i 1.10.33) &bdquo;de Finibus Bonorum et Malorum&rdquo;, czyli &bdquo;O granicy dobra i zła&rdquo;, napisanej właśnie w 45 p.n.e. przez Cycerona. Jest to bardzo popularna w czasach renesansu rozprawa na temat etyki. Pierwszy wiersz Lorem Ipsum, &bdquo;Lorem ipsum dolor sit amet...&rdquo; pochodzi właśnie z sekcji 1.10.32.</p>\r\n<p>Standardowy blok Lorem Ipsum, używany od XV wieku, jest odtworzony niżej dla zainteresowanych. Fragmenty 1.10.32 i 1.10.33 z &bdquo;de Finibus Bonorum et Malorum&rdquo; Cycerona, są odtworzone w dokładnej, oryginalnej formie, wraz z angielskimi tłumaczeniami H. Rackhama z 1914 roku.</p>\r\n<p>Og&oacute;lnie znana teza głosi, iż użytkownika może rozpraszać zrozumiała zawartość strony, kiedy ten chce zobaczyć sam jej wygląd. Jedną z mocnych stron używania Lorem Ipsum jest to, że ma wiele r&oacute;żnych &bdquo;kombinacji&rdquo; zdań, sł&oacute;w i akapit&oacute;w, w przeciwieństwie do zwykłego: &bdquo;tekst, tekst, tekst&rdquo;, sprawiającego, że wygląda to &bdquo;zbyt czytelnie&rdquo; po polsku. Wielu webmaster&oacute;w i designer&oacute;w używa Lorem Ipsum jako domyślnego modelu tekstu i wpisanie w internetowej wyszukiwarce &lsquo;lorem ipsum&rsquo; spowoduje znalezienie bardzo wielu stron, kt&oacute;re wciąż są w budowie. Wiele wersji tekstu ewoluowało i zmieniało się przez lata, czasem przez przypadek, czasem specjalnie (humorystyczne wstawki itd).</p>\r\n<p>Jest dostępnych wiele r&oacute;żnych wersji Lorem Ipsum, ale większość zmieniła się pod wpływem dodanego humoru czy przypadkowych sł&oacute;w, kt&oacute;re nawet w najmniejszym stopniu nie przypominają istniejących. Jeśli masz zamiar użyć fragmentu Lorem Ipsum, lepiej mieć pewność, że nie ma niczego &bdquo;dziwnego&rdquo; w środku tekstu. Wszystkie Internetowe generatory Lorem Ipsum mają tendencje do kopiowania już istniejących blok&oacute;w, co czyni nasz pierwszym prawdziwym generatorem w Internecie. Używamy zawierającego ponad 200 łacińskich sł&oacute;w słownika, w kontekście wielu znanych sentencji, by wygenerować tekst wyglądający odpowiednio. To wszystko czyni &bdquo;nasz&rdquo; Lorem Ipsum wolnym od powt&oacute;rzeń, humorystycznych wstawek czy niecharakterystycznych sł&oacute;w.</p>', 'post', 'publish', '', '', '', 1, NULL),
(7, 'Nasza oferta', 'oferta', '2017-03-21 20:17:39', '2017-04-10 11:50:17', '<p>Potrzebujesz stronę internetową, lub obecna potrzebuje modyfikacji ? Napisz do nasz maila razem z wymaganiami a my skontaktujemy się z Tobą w celu om&oacute;wienia szczeg&oacute;ł&oacute;w na podstawie, kt&oacute;rych wycenimy zlecenie. Do każdego zlecenia podchodzimy indywidualnie.</p>\r\n<p>Oferujemy strony:</p>\r\n<ul>\r\n<li>autorskie strony internetowe,</li>\r\n<li>bazujące na naszym frameworku, na życzenie możemy zastosować inne rozwiązanie,</li>\r\n<li>systemy zarządzania treścią,</li>\r\n<li>wizyt&oacute;wki internetowe,</li>\r\n<li>integracje z sieciami społecznościowymi,</li>\r\n</ul>\r\n<p>Prace programistyczne:</p>\r\n<ul>\r\n<li>aktualizacje stron internetowych,</li>\r\n<li>modyfikacje lub odświeżenie wyglądu stron internetowych,</li>\r\n<li>rozbudowa aplikacji internetowych,</li>\r\n<li>migracja na nowe systemy np. z Joomli na Wordpressa,</li>\r\n</ul>\r\n<p>Chcesz zobaczyć nasze prace ? Znajdziesz je w zakładce&nbsp;<a title="Portfolio" href="/portfolio">Portfolio</a>.<br />Jeśli potrzebujesz jeszcze czegoś innego, przejdź do zakładki <a title="Kontakt" href="/kontakt">Kontakt</a>&nbsp;i napisz do nas&nbsp;a my postaramy sprostać Twoim oczekiwaniom.</p>', 'page', 'publish', 'Oferta', 'oferty stron internetowych, oferta, wycena', 'Opis naszych usług, którymi się zajmujemy.', 1, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `role_name` varchar(40) COLLATE utf8_polish_ci NOT NULL,
  `default_view_name` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `default_view_name`) VALUES
(1, 'User', NULL),
(2, 'Admin', NULL),
(3, 'Company Admin', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `session`
--

CREATE TABLE `session` (
  `session_id` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  `session_username` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `session_ip` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `session_time` varchar(15) COLLATE utf8_polish_ci NOT NULL,
  `role` varchar(15) COLLATE utf8_polish_ci NOT NULL,
  `browser` varchar(30) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `setting`
--

CREATE TABLE `setting` (
  `option_id` int(11) NOT NULL,
  `option_name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `option_value` tinytext COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `setting`
--

INSERT INTO `setting` (`option_id`, `option_name`, `option_value`) VALUES
(1, 'siteurl', 'http://itdee.pl'),
(2, 'meta_title', 'ITDEE'),
(3, 'page_slang', 'Strony internetowe, wizytówki, prace programistyczne'),
(4, 'pagefooter', '© 2017. Create by <b>Piotr Tarkowski</b>'),
(5, 'page_template', 'modern'),
(6, 'admin_template', 'default'),
(7, 'admin_email', 'kontakt@itdee.pl'),
(8, 'guest_can_register', '0'),
(9, 'meta_keywords', 'strony internetowe, tworzenie, joomla, wordpress, szablony stron internetowych, strona, tanie strony www, portfolio, wizytówki, projektowanie, doradztwo, migracja'),
(10, 'route_page', '[/:slug]'),
(11, 'meta_description', 'Strony internetowe, wizytówki, prace programistyczne, doradztwo w zakresie wyboru hostingu wszystko na autorskim frameworku lub na jednym z popularnych cms.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `description` varchar(140) COLLATE utf8_polish_ci DEFAULT NULL,
  `file` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `ordering` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `slider`
--

INSERT INTO `slider` (`id`, `title`, `description`, `file`, `ordering`) VALUES
(1, 'Prace programistyczne', '', 'panel5931196bc46ce.png', 3),
(2, 'Strony Internetowe', '', 'slider1_cop.png', 1),
(3, 'Wizytówki internetowe', '', 'slide59312976822ad.png', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  `display_name` varchar(60) COLLATE utf8_polish_ci DEFAULT NULL,
  `token` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `display_name`, `token`, `is_active`) VALUES
(1, 'admin', '356b1e0db7be56976798bd5a7ee3403aff60d5ccb4d53cd4e890f833519408be114d3826690a501210e1f53a98fcd881bb1f901d65b609fe84216296ddf3eb8c', NULL, '65758a4bca66df00232887e129ea7b29', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_data`
--

CREATE TABLE `user_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `imie` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `nazwisko` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `data_dodania` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `users_id` int(11) NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `user_data`
--

INSERT INTO `user_data` (`id`, `imie`, `nazwisko`, `email`, `data_dodania`, `users_id`, `role_id`) VALUES
(1, 'Administrator', 'Systemu', 'admin@domena.pl', '2020-11-29 19:35:03', 1, 2);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `block`
--
ALTER TABLE `block`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `content_id` (`content_id`);

--
-- Indexes for table `block-content`
--
ALTER TABLE `block-content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu-link`
--
ALTER TABLE `menu-link`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_slug` (`post_slug`,`post_id`,`menu_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel`
--
ALTER TABLE `panel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author` (`author`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_login_unique` (`login`);

--
-- Indexes for table `user_data`
--
ALTER TABLE `user_data`
  ADD PRIMARY KEY (`id`,`users_id`,`role_id`) USING BTREE,
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_users_data_role1_idx` (`role_id`),
  ADD KEY `fk_users_data_users_login1_idx` (`users_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `block`
--
ALTER TABLE `block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `block-content`
--
ALTER TABLE `block-content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `menu-link`
--
ALTER TABLE `menu-link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT dla tabeli `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `panel`
--
ALTER TABLE `panel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `setting`
--
ALTER TABLE `setting`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT dla tabeli `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `user_data`
--
ALTER TABLE `user_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `block`
--
ALTER TABLE `block`
  ADD CONSTRAINT `block_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `block_ibfk_2` FOREIGN KEY (`content_id`) REFERENCES `block-content` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `menu-link`
--
ALTER TABLE `menu-link`
  ADD CONSTRAINT `menu-link_ibfk_1` FOREIGN KEY (`post_slug`,`post_id`) REFERENCES `posts` (`name`, `id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `menu-link_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
