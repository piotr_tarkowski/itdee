<?php
/*
 * main autoload:
 * ~Vendor
 */
function autoload_module($className)
{
    
    $file = str_replace("\\", "/", $className) . '.php';
    $dir = ROOTPATH . 'vendor' . DIRECTORY_SEPARATOR;
    if(file_exists($dir . $file))
    {
        
        require_once($dir . $file);
    }
    
    
}

spl_autoload_register('autoload_module');

