<?php 
if(!isset($config))
{
    $config = include 'config.php';
}
$dir = $config['upload_dir'];
define('ROOTPATH', str_replace('\\', '/', dirname(__DIR__) . '/')); 

//echo DIR;
//$dir = 'uploads/images/';



?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css"  href="<?= $config['base_url'] ?>/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css"  href="<?= $config['base_url'] ?>/css/font-awesome.css" />

        

        <script type="text/javascript" src="<?= $config['base_url'] ?>/js/jquery-3.1.1.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.js"></script>
        <script type="text/javascript" src="<?= $config['base_url'] ?>/js/bootstrap.js"></script>
        <style>
            label.mod > input{ /* HIDE RADIO */
                visibility: hidden; /* Makes input not-clickable */
                position: absolute; /* Remove input from document flow */
            }
            label.mod > input + img{ /* IMAGE STYLES */
                cursor:pointer;
                border:2px solid transparent;
                width: 200px;
                height: 130px;
            }
            label.mod > input:checked + img { /* (RADIO CHECKED) IMAGE STYLES */
                border:2px solid #f00;
            }

            label.mod img:hover {
                opacity: 0.3;
                border:2px solid #f00;
            }

            #radi:hover {



            }
            #mybtn {
                color: #fff;
                border: 1px solid #ccc;
                background-color: #2d8ac7;
                margin:5px;padding:5px;
                cursor: pointer;
            }
            #mybtn:hover {
                background-color: #2d7cc7;
            }
            .tab-content {
                padding: 10px;
            }
            .form-control {
                height: auto;
            }
            #radi {
                -webkit-box-shadow: 4px 4px 5px 0px rgba(0,0,0,0.75);
                -moz-box-shadow: 4px 4px 5px 0px rgba(0,0,0,0.75);
                box-shadow: 4px 4px 5px 0px rgba(0,0,0,0.75);
            }
            .tltImage {
                /*display: none;*/
                text-align: center;
            }
            .imgr {
                float:left;
                width:205px;
                min-height: 190px;
                border: 1px solid #000;
                margin:5px;
            }

            .imgTitle {
                padding:5px;

            }
            a#checkImg:hover span {

            }
        </style>

    </head>
    <body ng-app="productsApp" ng-controller="products">

        <ul class="nav nav-tabs" id="menuImg">
            <li class="active"><a data-toggle="tab" href="#allimg"><span class="fa fa-picture-o"></span> Wszystkie pliki</a></li>
            <li><a data-toggle="tab" href="#upload"><span class="fa fa-upload"></span> Wgraj plik</a></li>

        </ul>

        <div class="tab-content" style="margin-bottom: 60px" >
            <div class="alert alert-success" role="alert" id="alert" style="display:none" >
                <button type="button" class="close" ng-click="closeal()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div id="txt">Test</div>
            </div>
            <div id="allimg" class="tab-pane fade in active" style="overflow: auto" >

			
                <div class="imgr" id="radi" style="" ng-repeat="(key, value) in names track by $index" ng-if="value != '.' && value != '..'">
                    <div class="imgTitle">{{ value}}</div>
                    <label class="mod" name="mod-">
                        <input type="radio" name="img" value="{{ value}}" />
                        <img src="<?= $dir; ?>{{ value}}">
							
                    </label>

                    <div class="tltImage" id="" style="">
<!--                        <a href="#" id="" title="Pobierz"  class="dlImg" ><span class="fa fa-download" style="font-size: 20px"></span></a>-->
                        <a href="#" id="" title="Usuń"  ng-click="delImage(value, key)" ><span class="fa fa-times-circle" style="font-size: 20px"></span></a>
                    </div>
                </div> 

                <?php
                //}
//echo '<pre>' . print_r($files, 1) . '</pre>';
                ?>
                <div class="" style="background-color: #fff;text-align:right;left:0px;position: fixed;bottom: 0;width:100%;border-top: 1px solid #c5c5c5;;padding:10px 10px;">
                    <button id="btnInsert" ng-click="insImage()" class="btn btn-primary" type="button" style="">Wstaw</button>
                    <button id="btnClose" class="btn btn-default" ng-click="closeBrowser()" type="button" style="">Zamknij</button>
                </div>
            </div>
            <div id="upload" class="tab-pane fade">
                <?php
                ?>
                <h3>Wgrywanie nowego pliku</h3>


                <form ng-submit="submit()" name="form" role="form">
                    <div class="form-group">
                        <label>Wybierz plik</label>
                        <input ng-model="form.image" type="file" id="frmFile" class="form-control input-lg" accept="image/*" onchange="angular.element(this).scope().uploadedFile(this)" style="width:400px" >
                    </div>
                    <div class="form-group">
                        <label>Podgląd</label>
                        <img ng-src="{{image_source}}" style="width:300px;">
                    </div>
                    <!--                    <div class="form-group">        
                                            <input type="submit" id="submit" value="Submit" />
                                        </div>-->


                    <div class="" style="background-color: #fff;text-align:right;left:0px;position: fixed;bottom: 0;width:100%;border-top: 1px solid #c5c5c5;;padding:10px 10px;">
                        <button id="" type="submit" class="btn btn-primary" style="">Wyślij</button>
                        <button id="btnClose" ng-click="closeBrowser()" class="btn btn-default" type="button" style="">Zamknij</button>
                    </div>

                </form>






            </div>
        </div>
<div id="err_tes">
</div>
        <script>
                    var app = angular.module('productsApp', []);
                    

                    app.controller('products', function ($scope, $http, $window) {
						
						$scope.names = [];
                        $http.get("<?= $config['base_url']; ?>/admin/filemanager/imageList").then(function (response) {
                            //alert(response.data);
							//console.log(response.data);
							
							//$scope.names = $scope.arr(response.data);
							$scope.names = Object.keys(response.data).map(function(x) { return response.data[x]; })
                        });

                        //$scope.prod = {};
                        $scope.form = [];
                        $scope.files = [];
						
						
						
                        $scope.submit = function () {
                            $scope.form.image = $scope.files[0];

                            $http({
                                method: 'POST',
                                url: "<?= $config['base_url']; ?>/admin/filemanager/upload",
                                processData: false,
                                transformRequest: function (data) {
                                    var formData = new FormData();
                                    formData.append("image", $scope.form.image);
                                    return formData;
                                },
                                data: $scope.form,
                                headers: {
                                    'Content-Type': undefined
                                }
                            }).then(function (data) {
								//$window.alert(data.data.message);
								//$window.alert(data.data.status);
								//$window.alert(data.data.file);
                                //alert(data.file);
								
                                if(data.data.status == 1 )
                                {
                                    $scope.names.push(data.data.file);
                                    $('#frmFile').val('');
                                    $scope.image_source = '';
                                    $('div#alert').removeClass('alert-danger').addClass('alert-success');
                                    $('div#alert').slideDown().delay(5000).slideUp();
                                    //$('div.alert').addClass('alert-success');
                                    $('div#alert #txt').text(data.data.message);
                                }
                                if(data.data.status == 0 )
                                {
                                    $('div#alert').removeClass('alert-success').addClass('alert-danger');
                                    $('div#alert').slideDown().delay(5000).slideUp();
                                    $('div#alert #txt').text(data.data.message);
                                }
                                

                            });

                        };

                        $scope.uploadedFile = function (element) {
                            $scope.currentFile = element.files[0];
                            var reader = new FileReader();

                            reader.onload = function (event) {
                                $scope.image_source = event.target.result;
                                $scope.$apply(function ($scope) {
                                    $scope.files = element.files;
                                });
                            };
                            reader.readAsDataURL(element.files[0]);
                        };

                        $scope.delImage = function (code, index) {
                            //alert(code);
							//console.log($scope.names);
							//$window.alert(code);
	//$window.alert($scope.names);

                            
							
							var dat = { name: code };
                            $http({
								url: "<?= $config['base_url']; ?>/admin/filemanager/delete",
								method: 'POST',
								dataType: 'json',
								headers: {
									'Content-Type': 'application/json'
								},
								data: dat,
								//transformRequest: function(){},
								cache: false
							}).then(function (data) {
                                

                                if (data.data.status === 1)
                                {
									$scope.names.splice(index,1);
									//delete $scope.names[index];
                                }
                            });
                        };

                        $scope.insImage = function () {
                            var url = $('div#allimg').find('input[type="radio"]:checked').val();
							
                            if (url) {
                                top.tinymce.activeEditor.windowManager.getParams().oninsert(url);
                            } else {
                                top.tinymce.activeEditor.windowManager.alert('Nie wybrano obrazu.');
                            }
                        };
                        
                        $scope.closeal = function(){
                            $('div#alert').slideUp();
                        }

                        $scope.closeBrowser = function () {
                            top.tinymce.activeEditor.windowManager.close();
                        };

                    });



        </script>
        <script>

        </script>
    </body>
</html>