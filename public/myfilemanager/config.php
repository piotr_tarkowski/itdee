<?php

return array(
    'base_url' => 'http://itdee.pl',
    'upload_dir' => '/img/upload/',
    'ext_img' => [ 'jpg', 'jpeg', 'gif', 'png', 'tiff', 'bmp' ],
    'ext_video' => [ 'mov', 'mpeg', 'm4v', 'mp4', 'avi', 'mpg', 'wma', "flv", "webm" ],
    
);

