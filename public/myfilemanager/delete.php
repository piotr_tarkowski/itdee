<?php
    if(!isset($config))
    {
        $config = include 'config.php';
    }
    $dir = $config['upload_dir'];

    $file = filter_input(INPUT_GET, 'code');
    //$dir = 'uploads/images/';
    $path = realpath($dir . $file);
    

    unlink($path);
    echo json_encode(['status' => 1, 'message' => 'Plik został usunięty']);
    exit();
