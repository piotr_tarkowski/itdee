<?php
define('ROOTPATH', str_replace('\\', '/', dirname(__DIR__) . '/'));
DEFINE('TEMPLATE_PATH', ROOTPATH . 'module/Application/view/template/');



require('autoload.php');

use MVC\Application;
use MVC\App;

error_reporting(E_ALL);
ini_set('display_errors', '1');

/*
 * $app <- odpowiedzialna jest za uruchomienie całej applikacji 
 */
$app = new Application();    
/*
 * $appConfig <- moduły
 */
$appConfig = require ROOTPATH . 'config/application.config.php';
if(isset($appConfig))
{
    $app->setConfig('config' ,$appConfig);
}

$app->run();

return new App();



