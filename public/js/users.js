
$(function () {
    $(".search").keyup(function ()
    {
        var searchid = $(this).val();
        var dataString = 'search=' + searchid;
        if (searchid.length === 0)
        {
            $("#result").hide();
            $("#tblUsers tbody tr").show();
        }
        if (searchid.length > 0)
        {
            $.ajax({
                type: "POST",
                url: "/generator/admin/users/search",
                data: dataString,
                cache: false,
                success: function (html)
                {
                    //alert(html);
                    var sa = $.parseHTML(html)
                    $("#result").html(sa).show();

                }
            });
        }
        return false;
    });
});

$(document).on("click", function (e) {
    var clicked = $(e.target);

    if (!clicked.hasClass("search")) {
        $("#result").hide();
        //$(".search").val('');
    }
    if (clicked.hasClass("btn-search"))
    {
        var id = $(clicked).attr('id');
        //alert(id);
        $(".search").val(clicked.text());
        $('#tblUsers tbody tr').each(function () {
            $(this).show();
            if ($(this).attr('id') !== id)
            {
                $(this).hide();

            }
        });
    }
    if (clicked.hasClass('clsFiltr'))
    {
        $('#tblUsers tbody tr').each(function () {

            $(".search").val('');
            $(this).show();


        });
    }
});



win
window.onLoad(function () {
    var modal = $('#myModal');


    $('.btn-edit').click(function () {
        modal.modal('show');
        var id = $(this).attr('id');
        //alert('fdhf');
        $('#btn-save').val('Save').prop('name', 'save').text('Save');

        //alert(d);


        $.ajax({

            //crossDomain: true,
            url: '/generator/admin/users/index/' + id,
            type: 'get',
            //data: {id: id},
            dataType: "json",
            success: function (data) {

                if (data.status === 1) {
                    $('h4.modal-title').text('Edycja Użytkownika');
                    $("input#uid").val(data.user.id),
                            $("input#udid").val(data.user.udid),
                            //$("input#name").val(data.user.login);

                            $("input#name").prop('disabled', true).val(data.user.login);
                    //$("input#name").parent(this).removeClass("form-group-sm").addClass('form-group-lg');


                    $("input#email").val(data.user.email);
                    $("input#fname").val(data.user.imie);
                    $("input#sname").val(data.user.nazwisko);
                    $("#role option").each(function () {
                        if ($(this).val() === data.user.rola_id)
                        {
                            $(this).prop('selected', true);
                        }
                    });

                    if (data.user.is_active === '1')
                    {
                        $("input#active").prop('checked', true);
                    } else {
                        $("input#active").prop('checked', false);
                    }
                    //

                } else {
                    alert(data.message);
                }
            }
        });

    });

    $('#btn-new').click(function () {
        modal.modal('show');

        $('#myForm input').val('');
        if ($('#active').prop('checked'))
        {
            $('#active').prop('checked', false);

        }


        $("input#name").prop('disabled', false);


        $('select#role').val(1);
        $('h4.modal-title').text('Nowy użytkownik');
        $('#btn-save').val('Dodaj').prop('name', 'dodaj').text('Dodaj');



    });

    $('.btn-usun').click(function () {

        $.post("/generator/admin/users/usun", {id: $(this).attr('id'), delete: true}, function (result) {
            var data = $.parseJSON(result);
            if (data.status === 1)
            {
                //alert(dat.message);
                location.reload();
            }
        });

    });

    $('#btn-save').click(function () {
        var btn = $('#btn-save');

        if ($('#active').prop('checked'))
        {
            $('#active').prop('value', 1);
        } else {
            $('#active').prop('value', 0);
        }
        //alert($('#active').val());

        if ($('#btn-save').attr('name') === 'dodaj')
        {

            if ($('#name').val() === '' || $('#passwd').val() === '' || $('#email').val() === '')
            {
                $('#err-user').slideDown(300);
                $('#err-user').text('trzeba wypełnić obowiązkowe pola.');
            } else {
                $.post("/generator/admin/users/nowy",
                        {
                            login: $('#name').val(),
                            passwd: $('#passwd').val(),
                            active: $('#active').val(),
                            email: $('#email').val(),
                            fname: $('#fname').val(),
                            sname: $('#sname').val(),
                            role: $('#role').val(),
                            new_user: $('#btn-save').val()
                        }, function (result) {
                    var data = $.parseJSON(result);
                    if (data.status === 1)
                    {
                        //alert(dat.message);
                        location.reload();
                    }

                });
            }

        } else if ($('#btn-save').attr('name') === 'save')
        {


            $.post("/generator/admin/users/edycja",
                    {
                        uid: $('#uid').val(),
                        active: $('#active').val(),
                        login: $('#name').val(),
                        passwd: $('#passwd').val(),
                        email: $('#email').val(),
                        fname: $('#fname').val(),
                        sname: $('#sname').val(),
                        role: $('#role').val(),
                        udid: $('#udid').val(),
                        save: $('#btn-save').val()
                    }, function (result) {
                var data = $.parseJSON(result);
                if (data.status === 1)
                {
                    //alert(dataa.message);
                    location.reload();
                }
            });
        }
    });

});