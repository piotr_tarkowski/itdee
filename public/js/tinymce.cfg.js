


    tinymce.init({
        selector: '#editContent',
        theme: 'modern',
        plugins : 'advlist autolink link image  imagetools media paste lists charmap preview emoticons wordcount visualblocks visualchars code hr anchor pagebreak spellchecker textcolor table contextmenu responsivefilemanager ',
        
        toolbar: 'undo redo | formatselect, fontselect, fontsizeselect, forecolor | bold, italic, underline, hr | alignleft, aligncenter, alignright, alignjustify | bullist, numlist, outdent, indent | link image media | table | preview visualblocks  ', //tabela; => tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol
        
        
    
        menubar: false,
        
        language: 'pl',
        //'8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt'
        fontsize_formats: '8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt',
        font_formats: 'Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Verdana=verdana,geneva;',
        
        height: 600,
        max_height: 600,
        
        block_formats: 'Paragraph=p;Div=div;Heading 1=h1;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Preformatted=pre',
        
        browser_spellcheck: true,
        visualblocks_default_state: true,
        schema: 'html5',
        
        image_advtab: true,
        image_title: true,
        
        external_filemanager_path:"/myfilemanager/",
        //filemanager_title:"Wybierz lub wgraj plik" ,
        //external_plugins: { "myfilemanager" : "/myfilemanager/plugin.min.js"},
        
        //images_upload_url: 'http://localhost/dobra/admin/posts/upload',
        
        //images_upload_base_path: "http://localhost/dobra/",
		image_prepend_url: "/img/upload/",
        images_upload_base_path: "/img/upload/",
        
        automatic_uploads: true,
        file_picker_types: 'image media',
        
        
        paste_data_images: true,

        convert_urls : false,
        
        
        images_reuse_filename: true,
        images_upload_credentials: true,
        file_browser_callback: function(field_name, url, type, win) {

            var a = tinyMCE.settings.external_filemanager_path;
            var b = tinyMCE.settings.images_upload_base_path;
            //alert(a + ' ' + b);
            //var baseURL = "/open/uploads/images/";
            var baseURL = tinyMCE.settings.images_upload_base_path;
            var uri = 'http://itdee.pl';    // script URL - use an absolute path!
            var cmsURL = '/myfilemanager/';
            
            if (cmsURL.indexOf("?") < 0) {
                cmsURL = cmsURL + "filemanager.php?type=" + type + "&uploads=" + baseURL;
            }
            else {
                cmsURL = cmsURL + "&type=" + type + "&upload=" + baseURL;
            }
			
            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                autoScroll: false,
                scrollbars: 'hidden',
                title : 'File manager',
                width : 800,
                height : 600,
                resizable : true,
                inline : true,
                close_previous : false
                

            }, {
                window : win,
                input : field_name,
                oninsert: function(url) {
					
                    win.document.getElementById(field_name).value = uri + baseURL + url; 
                    tinymce.activeEditor.windowManager.close();
                },
                onselect: function() {
                    // 
                }
            });
            return false;
        }

        
        
    });

